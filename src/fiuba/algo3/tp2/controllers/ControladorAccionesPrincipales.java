package fiuba.algo3.tp2.controllers;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fiuba.algo3.tp2.entities.Juego;
import fiuba.algo3.tp2.entities.Persistencia;
import fiuba.algo3.tp2.entities.Policia;
import fiuba.algo3.tp2.views.PanelAccionesViajar;
import fiuba.algo3.tp2.views.VentanaPrincipal;

public class ControladorAccionesPrincipales implements ActionListener{

	private Juego juego;
	private VentanaPrincipal vista;
	private Policia policia;
	private ControladorViajar controladorViajar;

	public ControladorAccionesPrincipales(Juego juego,VentanaPrincipal ventanaPrincipal, Policia policia) {
		this.juego = juego;
		this.vista = ventanaPrincipal;
		this.policia = policia;
		controladorViajar = new ControladorViajar(juego, ventanaPrincipal);
		
	}

	public void actionPerformed(ActionEvent e) {
		
		if("Viajar".equalsIgnoreCase(e.getActionCommand())){
			
			vista.activarAccionesViajar();
			vista.mostrar(new PanelAccionesViajar(juego,controladorViajar), BorderLayout.SOUTH);	
		}
		else{
			if("Capitales Conectadas".equalsIgnoreCase(e.getActionCommand())){
				vista.activarCapitalesConectadas();	
			}
			else{
				if("Visitar Lugar".equalsIgnoreCase(e.getActionCommand())){
					vista.activarAccionesVisitarLugar();
				}
				else{
					if("Cerrar Caso".equalsIgnoreCase(e.getActionCommand())){
						vista.iniciarNuevoJuego();
					}
					else{
						if("Guardar".equalsIgnoreCase(e.getActionCommand())){
							Persistencia persistencia = new Persistencia();
							persistencia.guardarArchivo(juego);
							vista.informarGuardado();
							}
						}
					}
				
				}
			
			}
		
	}

	public void setJuego(Juego juegoActual) {
		juego = juegoActual;
		policia = juegoActual.getPolicia();
		controladorViajar.setJuego(juegoActual);
	}
	
		
}
