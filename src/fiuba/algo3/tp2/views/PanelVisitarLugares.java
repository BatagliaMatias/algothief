package fiuba.algo3.tp2.views;


import java.awt.Color;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import fiuba.algo3.tp2.controllers.ControladorAccionesVisitarLugares;

public class PanelVisitarLugares extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private ControladorAccionesVisitarLugares controlador;

	public PanelVisitarLugares(ControladorAccionesVisitarLugares controladorAccionesLugares) {
		

		this.controlador = controladorAccionesLugares;
		
		setVisible(true);
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		setBackground(Color.LIGHT_GRAY);
		
		JButton botonCalle = crearBoton("Recorrer la calle",controladorAccionesLugares);
		JButton botonMuseo = crearBoton("Visitar Museo",controladorAccionesLugares);
		JButton botonAero =  crearBoton("Investigar el Aeropuerto",controladorAccionesLugares);
		JButton botonBanco =  crearBoton("Investigar Banco",controladorAccionesLugares);
		JButton botonAtras =  crearBoton("Volver al Menu Anterior",controladorAccionesLugares);
		
		this.add(botonCalle);
		this.add(botonMuseo);
		this.add(botonAero);
		this.add(botonBanco);
		this.add(botonAtras);
		
	}
	
	private JButton crearBoton(String nombre, ControladorAccionesVisitarLugares controladorAccionesLugares) { 
		JButton boton = new JButton(nombre);
		boton.setAlignmentX(CENTER_ALIGNMENT);
		boton.addActionListener(controladorAccionesLugares);
		return boton;
	}

}
