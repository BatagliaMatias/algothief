package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;


import java.util.Map;


import org.junit.Test;

public class LectorCapitalConectadasTest {

	@Test
	public void testBuscarConexionesRoma() {
		LectorCiudadConectadas lector = new LectorCiudadConectadas();
		Map<String, Integer>conexiones =  lector.leerConexiones("Roma");	
		assertTrue(conexiones.get("Paris").equals(1105));
		assertTrue(conexiones.get("Moscu").equals(2376));
		assertTrue(conexiones.get("Buenos Aires").equals(11125));				
		
		//java.util.Iterator it = conexiones.entrySet().iterator();
		//System.out.println(it.next());
	
	}

}
