package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

public class ObjetoRobadoTest {

	@Test
	public void testObjetoRobadoDevolverClases() {
		Capital capital = new Capital("Paris","Francia");
		ObjetoRobado objeto = new ObjetoRobado();
		objeto.leerObjetoDeCapital(capital);
		assertEquals(objeto.getNombreObjeto().getClass(),String.class);
	}

}
