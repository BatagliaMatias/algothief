package fiuba.algo3.tp2.views;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;


import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fiuba.algo3.tp2.controllers.Controlador;
import fiuba.algo3.tp2.controllers.ControladorSeleccionarPolicia;
import fiuba.algo3.tp2.entities.LectorPoliciaXml;

public class  PanelSeleccionPolicia   extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PanelSeleccionPolicia(){
		setVisible(true);
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		setBackground(Color.LIGHT_GRAY);
	
		
		
	}
	
	public void llenarDePolicias(Controlador control){
		
		this.removeAll();
		JLabel label = new JLabel("Partida Nueva");
		label.setAlignmentX(CENTER_ALIGNMENT);
		Font labelFont = label.getFont();
		label.setFont(new Font(labelFont.getName(), Font.PLAIN, 30));
		
		this.add(label);
		LectorPoliciaXml lector = new LectorPoliciaXml();
		ArrayList<String> policias = lector.getTodosLosPolicias();
		
		
		//ArrayList<String> policias = new ArrayList<String>();
		
		//policias.add("Mat");
		//policias.add("otro");
		
		for (int i = 0; i < policias.size();i++){
			this.add(crearBoton(policias.get(i),control.getControlSeleccionarPolicia()));
		}
	}
	
	private JButton crearBoton(String nombre, ControladorSeleccionarPolicia controladorSeleccionarPolicia) {
		
		JButton boton = new JButton(nombre);
		boton.setAlignmentX(CENTER_ALIGNMENT);
		boton.addActionListener(controladorSeleccionarPolicia);
		return boton;
	}

	
	
	
	
}
