package fiuba.algo3.tp2.entities;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fiuba.algo3.tp2.interfaces.Pista;

public class LectorPistaXml implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Pista leerPista(String ciudadBuscada, String dificultadBuscada, String TipoBuscado) {
		
		String pista = "";
		Document document;
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.parse(new File("xml/pistas.xml"));
			NodeList nodeList = document.getDocumentElement().getChildNodes();
			
			for (int i = 0; i < nodeList.getLength(); i++) {
               Node node = nodeList.item(i);
               if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element elem = (Element) node;
                    String ciudadXML = node.getAttributes().getNamedItem("ciudad").getNodeValue();
                    String dificultadXML = node.getAttributes().getNamedItem("dificultad").getNodeValue();
                    
                    if (ciudadXML.equals(ciudadBuscada) && dificultadXML.equals(dificultadBuscada)){
                    
                    	
                    	if (TipoBuscado.toString() == "economico"){
                    	pista = elem.getElementsByTagName(TipoBuscado).item(0).getChildNodes().item(0).getNodeValue();
                    	PistaEconomica pistaEco = new PistaEconomica();
                    	pistaEco.setDescripcion(pista);
                    	return pistaEco;
                    	}
          
                    	else if (TipoBuscado.toString() == "descriptivo"){
                        	pista = elem.getElementsByTagName(TipoBuscado).item(0).getChildNodes().item(0).getNodeValue();
                        	PistaInteres pistaDes = new PistaInteres();
                        	pistaDes.setDescripcion(pista);
                        	return pistaDes;
                    	}
                    	
                    	else if (TipoBuscado.toString() == "historico"){
                        	pista = elem.getElementsByTagName(TipoBuscado).item(0).getChildNodes().item(0).getNodeValue();
                        	PistaHistorica pistaHis = new PistaHistorica();
                        	pistaHis.setDescripcion(pista);
                        	return pistaHis;
                    	}
                    	
                
                    	
                    }
               }
			}
          
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//return pista;
		return null;

 }




}
