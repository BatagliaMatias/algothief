package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.entities.LadronGenerico;
import fiuba.algo3.tp2.entities.LadronGenerico.TipoRasgo;
import fiuba.algo3.tp2.exceptions.ExcepcionCapitalesNoConectadas;
import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;
import fiuba.algo3.tp2.interfaces.Viajable;


public class LadronGenericoTest {

	@Test
	public void testCrearLadronGenerico() {
		String sexo = "Femenino";
		String hobby = "Tenis";
		String cabello = "Rubio";
		String senia = "Anillo";
		String vehiculo = "Moto";
		String nombre = "Nick Brunch";
		String dificultad = "facil";
		LadronGenerico ladronGenerico = new LadronGenerico(nombre,sexo,hobby,cabello,senia,vehiculo,dificultad);
		assertEquals(ladronGenerico.getRasgo(TipoRasgo.cabello).getDescripcion(),"Rubio");
		

	}
	
	@Test
	public void testCrearLadronRasgos(){
		String sexo = "Femenino";
		String hobby = "Tenis";
		String cabello = "Rubio";
		String senia = "Anillo";
		String vehiculo = "Moto";
		String nombre = "Nick Brunch";
		String dificultad = "facil";
		LadronGenerico ladronGenerico = new LadronGenerico(nombre,sexo,hobby,cabello,senia,vehiculo,dificultad);
		assertEquals(ladronGenerico.getRasgo(TipoRasgo.sexo).getDescripcion(),"Femenino");
	}

	//@Test Este test deja pistas pero sin saber a donde va a viajar, estan mal las pistas
	public void testDejarPista(){ 
		Capital capital = new Capital("Roma","Italia");
		//Capital destino = new Capital("Ugugugug", "Somalia");
		
		String sexo = "Femenino";
		String hobby = "Tenis";
		String cabello = "Rubio";
		String senia = "Anillo";
		String vehiculo = "Moto";
		String nombre = "Nick Brunch";
		String dificultad = "facil";
		LadronGenerico ladronGenerico = new LadronGenerico(nombre,sexo,hobby,cabello,senia,vehiculo,dificultad);
		String esperadoEconomico = "El sospechoso compro euros";
		String esperadoDescriptivo = "El sospechoso viajo en un vehiculo con una bandera de colores verde, blanco y rojo";
		String esperadoHistorico = "El sospechoso buscaba libros de gladiadores";
		
		ladronGenerico.setUbicacion(capital);
		//ladronGenerico.dejarPista(capital);	necesita saber a donde va

		assertEquals(capital.getDescripcionPista(TipoLugar.economico), esperadoEconomico);
		assertEquals(capital.getDescripcionPista(TipoLugar.descriptivo), esperadoDescriptivo);
		assertEquals(capital.getDescripcionPista(TipoLugar.historico), esperadoHistorico);
		
		ladronGenerico.determinarNombreProximaCiudad();
	}
	
	@Test
	public void testViajar(){
		Capital destino = new Capital("Roma","Italia");   //Esta en Paris y viaja a Roma
		Capital capital = new Capital("Paris","Francia");
		String sexo = "Femenino";
		String hobby = "Tenis";
		String cabello = "Rubio";
		String senia = "Anillo";
		String vehiculo = "Moto";
		String nombre = "Nick Brunch";
		String dificultad = "facil";
		
		String esperadoEconomico = "El sospechoso compro euros";
		String esperadoDescriptivo = "El sospechoso viajo en un vehiculo con una bandera de colores verde, blanco y rojo";
		String esperadoHistorico = "El sospechoso buscaba libros de gladiadores";
		
		Viajable ladronGenerico = new LadronGenerico(nombre,sexo,hobby,cabello,senia,vehiculo,dificultad);	
		ladronGenerico.setUbicacion(capital);
		try {
			ladronGenerico.viajar(destino);
		} catch (ExcepcionCapitalesNoConectadas e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		assertEquals(capital.getDescripcionPista(TipoLugar.economico), esperadoEconomico);
		assertEquals(capital.getDescripcionPista(TipoLugar.descriptivo), esperadoDescriptivo);
		assertEquals(capital.getDescripcionPista(TipoLugar.historico), esperadoHistorico);		
		assertEquals(capital.getPista(TipoLugar.rasgo).getClass(),PistaRasgo.class);
		assertEquals(ladronGenerico.getUbicacion(), destino);
		assertEquals(((LadronGenerico)ladronGenerico).getRasgos().size(), 4);
	}
	
	@Test
	public void testCiudadRandom(){ 
		Capital capital = new Capital("Roma","Italia");
		//Capital destino = new Capital("Ugugugug", "Somalia");
		
		String sexo = "Femenino";
		String hobby = "Tenis";
		String cabello = "Rubio";
		String senia = "Anillo";
		String vehiculo = "Moto";
		String nombre = "Nick Brunch";
		String dificultad = "facil";
		LadronGenerico ladronGenerico = new LadronGenerico(nombre,sexo,hobby,cabello,senia,vehiculo,dificultad);
		
		ladronGenerico.setUbicacion(capital);
		//ladronGenerico.dejarPista(capital);	necesita saber a donde va

		
		String nombreCiudadRandom = ladronGenerico.determinarNombreProximaCiudad();
		
		assertTrue(ladronGenerico.getUbicacion().getConexiones().containsKey(nombreCiudadRandom));
	}
}
