package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.entities.Policia;
import fiuba.algo3.tp2.exceptions.ExcepcionCapitalesNoConectadas;
import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;
import fiuba.algo3.tp2.interfaces.Viajable;

public class PoliciaTest {

	
	final Integer velocidad_novato=900;
	final Integer velocidad_detective=1100;
	final Integer velocidad_investigador=1300;
	final Integer velocidad_sargento=1500;
	
	@Test
	public void testCrearPoliciaNuevoNovato() {
		Policia policia = new Policia("Felipe Gonzalez", 0);
		assertEquals(policia.rango(), "Novato");
		assertEquals(policia.getNombre(),"Felipe Gonzalez");
		assertEquals(policia.getVelocidad(),velocidad_novato);
		}

	@Test
	public void testPoliciaDeberiaSubirADetective(){
		Integer i=0;
		Policia policia = new Policia("Alex Lifeson", i);
		while (i<5){
			policia.arrestoConseguido();
			i++;
		}
		assertEquals(policia.rango(), "Detective");
		assertEquals(policia.getNombre(),"Alex Lifeson");
		assertEquals(policia.getVelocidad(),velocidad_detective);
		}
	
	@Test
	public void testPoliciaDeberiaSubirAInvestigador(){
		Integer i=0;
		Policia policia = new Policia("Geddy Lee", i);
		while (i<10){
			policia.arrestoConseguido();
			i++;
		}
		assertEquals(policia.rango(), "Investigador");
		assertEquals(policia.getNombre(),"Geddy Lee");
		assertEquals(policia.getVelocidad(),velocidad_investigador);
		}
	
	@Test
	public void testPoliciaDeberiaSubirASargento(){
		Integer i=0;
		Policia policia = new Policia("Neil Peart", i);
		while (i<20){
			policia.arrestoConseguido();
			i++;
		}
		assertEquals(policia.rango(), "Sargento");
		assertEquals(policia.getNombre(),"Neil Peart");
		assertEquals(policia.getVelocidad(),velocidad_sargento);
		}
	
	@Test
	public void testPoliciaViajar(){
		Viajable policia = new Policia("Sancho Panza",0);
		Capital ubicacion = new Capital("Paris","Francia");
		Capital destino = new Capital("Roma","Italia");
		policia.setUbicacion(ubicacion);
		try {
			policia.viajar(destino);
		} catch (ExcepcionCapitalesNoConectadas e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(policia.getUbicacion(),destino);
		
	}
	
	@Test
	public void testPoliciaViajarConTiempoNovato(){
		Policia policia = new Policia("Sancho Panza",0);
		Capital ubicacion = new Capital("Paris","Francia");
		Capital destino = new Capital("Roma","Italia");
		String ahora = "Lunes 9:00";
		policia.setUbicacion(ubicacion);
		try {
			policia.viajar(destino);
		} catch (ExcepcionCapitalesNoConectadas e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(policia.getUbicacion(),destino);
		assertEquals(policia.getHora(),ahora);
		
	}
	
	@Test
	public void testPoliciaViajarConTiempoAltoRango(){
		Policia policia = new Policia("Sancho Panza",20);
		Capital ubicacion = new Capital("Paris","Francia");
		Capital destino = new Capital("Roma","Italia");
		String ahora = "Lunes 8:00";
		policia.setUbicacion(ubicacion);
		try {
			policia.viajar(destino);
		} catch (ExcepcionCapitalesNoConectadas e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(policia.getUbicacion(),destino);
		assertEquals(policia.getHora(),ahora);
		
	}
	@Test
	public void testPoliciaViajarLargoConTiempoNovato(){
		Policia policia = new Policia("Sancho Panza",0);
		Capital ubicacion = new Capital("Buenos Aires","Argentina");
		Capital destino = new Capital("Roma","Italia");
		String ahora = "Lunes 20:00";
		policia.setUbicacion(ubicacion);
		try {
			policia.viajar(destino);
		} catch (ExcepcionCapitalesNoConectadas e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(policia.getUbicacion(),destino);
		assertEquals(policia.getHora(),ahora);
		
	}
	
	
	@Test
	public void testDormir(){
		Policia policia = new Policia("Dormilon",0);
		policia.dormir();
		String ahora = "Lunes 15:00";
		assertEquals(policia.getHora(), ahora);
		
	}
	
	@Test
	public void testInteractuarPistaDeUnLugar(){
		Policia policia = new Policia("policia",0);
		Escenario escenario = new Escenario();
		Capital roma = escenario.obtenerCapital("Roma");
		Capital paris = escenario.obtenerCapital("Paris");
		Capital buenosAires = escenario.obtenerCapital("Buenos Aires");
		assertEquals(paris.getPista(TipoLugar.economico).interactuar(policia),"No he visto a nadie sospechoso");
		assertEquals(roma.getPista(TipoLugar.economico).interactuar(policia),"No he visto a nadie sospechoso");
		assertEquals(buenosAires.getPista(TipoLugar.economico).interactuar(policia),"No he visto a nadie sospechoso");
	}
	
}

