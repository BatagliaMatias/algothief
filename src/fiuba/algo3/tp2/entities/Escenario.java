package fiuba.algo3.tp2.entities;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;
import fiuba.algo3.tp2.interfaces.Pista;

public class Escenario implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Capital> capitales;

	public Escenario(){
			
			capitales = new ArrayList<Capital>();
			
			Document document;
			
			try {
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				document = builder.parse(new File("xml/Capitales.xml"));
				NodeList nodeList = document.getDocumentElement().getChildNodes();
				
				for (int i = 0; i < nodeList.getLength(); i++) {
	               Node node = nodeList.item(i);
	               if (node.getNodeType() == Node.ELEMENT_NODE) {
	                  String nombreCapital = node.getAttributes().getNamedItem("NombreCapital").getNodeValue();                    
	                  String nombrePais = node.getAttributes().getNamedItem("NombrePais").getNodeValue();
	                  this.agregarCapital(nombreCapital,nombrePais);
	               }
				}
	          
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		
		}

	private void agregarCapital(String nombreCapital, String nombrePais) {
		Capital capital = new Capital(nombreCapital, nombrePais);
		Pista pista = new PistaVacia();
		capital.getLugar(TipoLugar.economico).setearPista(pista);
		capital.getLugar(TipoLugar.descriptivo).setearPista(pista);
		capital.getLugar(TipoLugar.historico).setearPista(pista);
		capital.getLugar(TipoLugar.rasgo).setearPista(pista);
		capitales.add(capital);
		
	}

	public Capital obtenerCapital(String nombreCapital) {
		boolean encontrado = false;
		Iterator<Capital> iterator = capitales.iterator();
		while(iterator.hasNext()&& !encontrado){
			Capital posible = iterator.next();
			if(posible.getNombreCapital().equals(nombreCapital)){
				return posible;
			}
		}
		return null;
	}
	
	
}
