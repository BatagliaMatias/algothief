package fiuba.algo3.tp2.entities;

import java.io.Serializable;

import fiuba.algo3.tp2.interfaces.Ladron;
import fiuba.algo3.tp2.interfaces.Viajable;

public class LadronDificil extends LadronGenerico implements Ladron, Viajable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final int MAXVIAJES = 7;
	
	public LadronDificil(String nombre, String sexo, String hobby,
			String cabello, String senia, String vehiculo) {
		super(nombre, sexo, hobby, cabello, senia, vehiculo,"dificil");
		contadorViajesDisponibles = MAXVIAJES;
	}

	
	public String objetoRobado() {
		return "Anillo del Papa";
	}

	public int rutaEscape() {
		return MAXVIAJES;
	}
	public void setContadorViajesDisponibles(int contadorViajesDisponibles) {
		this.contadorViajesDisponibles = contadorViajesDisponibles;
	}

	public boolean viajeDisponible() {
		if (this.contadorViajesDisponibles>0) return true;
		else return false;
	}
	
}
