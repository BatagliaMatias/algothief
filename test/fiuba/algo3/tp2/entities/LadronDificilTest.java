package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.exceptions.ExcepcionCapitalesNoConectadas;
import fiuba.algo3.tp2.interfaces.Ladron;

public class LadronDificilTest {

	@Test
	public void testIdentificarObjetoParaRobar() {
		Ladron ladronDificil = new LadronDificil(null, null, null, null, null, null);
		String botin = "Anillo del Papa";
		assertEquals(ladronDificil.objetoRobado(), botin);
		
	}
	
	@Test
	public void testRutaDeEscape(){
		Ladron ladronDificil = new LadronDificil(null, null, null, null, null, null);
		int cantidadCapitales = 7;
		assertEquals(ladronDificil.rutaEscape(), cantidadCapitales);
	}
	
	@Test
	public void testViajesDisponibleDificil(){
		LadronGenerico ladron = new LadronFacil(null, null, null, null, null, null);
		assertTrue(ladron.viajeDisponible());
		
		Capital capital = new Capital("Paris","Francia");
		Capital destino = new Capital("Roma","Italia");
		for(int i=0;i<7;i++){
		ladron.setUbicacion(capital);
		try {
			ladron.viajar(destino);
			ladron.restarContador();
		} catch (ExcepcionCapitalesNoConectadas e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		}
				
		assertFalse(ladron.viajeDisponible()); 
		
	}
	
}
