package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.entities.Juego;
import fiuba.algo3.tp2.entities.Policia;
import fiuba.algo3.tp2.interfaces.Ladron;
import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;

public class JuegoTest {

	@Test
	public void testCrearJuego() {
		Juego juego = new Juego();
		assertNotNull(juego);
	}
	
	@Test
	public void testJuegoIntroduccion(){
		
		Juego juego = new Juego();
		assertEquals(juego.saludar(),"Bienvenido Policia al teclado, por favor identifiquese" );
		
	}
	
	@Test
	public void testValidarPolicias(){
		
		Juego juego = new Juego();
		Policia policia = new Policia("gonzalez", 0);
		juego.agregarAgente(policia);
		assertTrue(juego.validarAgente(policia));
			
	}

	@Test
	public void testJuegoInicializaCasoADificultadFacilEnRomaYLadronViajoAParis(){
		Juego juego = new Juego ();		
		int casosPolicia = 0;
		Policia policia = new Policia("gonzalez", casosPolicia);
		Capital roma = new Capital("Roma", "Italia");
		Capital paris = new Capital("Paris","Francia");
		
		juego.agregarAgente(policia);
		juego.crearCaso();
		juego.ladronRobaObjetoEn(roma); //inicia en Roma, no deja pistas en ningun lado
		juego.policiaViajaA(roma);
		juego.ladronViajaA(paris); //viaja a Paris, deja pistas sobre Paris en Roma
			
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"El sospechoso viajo a un pais con una bandera roja, azul y blanca");
		assertEquals(juego.devolverPista(TipoLugar.historico),"El sospechoso buscaba recetas para preparar baguettes");
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso compro francos" );
	}
	
	
	@Test
	public void testJuegoPoliciaViajaAUnLugarDondeElLadronNoEstuvo(){
		
		Juego juego = new Juego ();		
		int casosPolicia = 0;
		Policia policia = new Policia("gonzalez", casosPolicia);
		Capital roma = new Capital("Roma", "Italia");
		Capital buenosAires = new Capital("Buenos Aires", "Argentina");
		Capital rusia = new Capital("Moscu", "Rusia");
		juego.agregarAgente(policia);
		juego.crearCaso();
		juego.ladronRobaObjetoEn(roma);
		juego.policiaViajaA(roma);
		juego.ladronViajaA(buenosAires);
		juego.policiaViajaA(rusia);
		
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"No he visto a nadie sospechoso");
		assertEquals(juego.devolverPista(TipoLugar.economico),"No he visto a nadie sospechoso");
		assertEquals(juego.devolverPista(TipoLugar.historico),"No he visto a nadie sospechoso" );
	}
	
	@Test
	public void testJuegoPoliciaViajaAUnLugarDondeElLadronNoEstuvoYSeReinvidica(){
		
		Juego juego = new Juego ();		
		int casosPolicia = 11;
		Policia policia = new Policia("gonzalez", casosPolicia);
		Capital paris = new Capital("Paris", "Francia");
		Capital buenosAires = new Capital("Buenos Aires", "Argentina");
		Capital roma = new Capital("Roma","Italia");
		
		juego.agregarAgente(policia);
		juego.crearCaso();
		
		juego.ladronRobaObjetoEn(roma);
		juego.policiaViajaA(roma);
		juego.ladronViajaA(buenosAires);
		juego.policiaViajaA(paris);
		
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"No he visto a nadie sospechoso");
		assertEquals(juego.devolverPista(TipoLugar.economico),"No he visto a nadie sospechoso");
		assertEquals(juego.devolverPista(TipoLugar.historico),"No he visto a nadie sospechoso" );
	
		juego.policiaViajaA(buenosAires);
		juego.ladronViajaA(paris);
		
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"El sospechoso viajo a un pais con una bandera roja, azul y otro color");
		assertEquals(juego.devolverPista(TipoLugar.historico),"El sospechoso compro un boleto para viajar en el TGV");
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso compro euros" );
	}
	
	
	@Test
	public void testJuegoInicializaCasoADificultadMediaEnParis(){
		Juego juego = new Juego ();		
		int casosPolicia = 11;
		Policia policia = new Policia("gonzalez", casosPolicia);
		Capital paris = new Capital("Paris", "Francia");
		Capital roma = new Capital("Roma","Italia");
		
		juego.agregarAgente(policia);
		juego.crearCaso();
		
		juego.ladronRobaObjetoEn(roma);
		juego.policiaViajaA(roma);
		juego.ladronViajaA(paris);
	
		assertEquals(juego.devolverObjetoRobado(),"Famoso huevo de Faberge");
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"El sospechoso viajo a un pais con una bandera roja, azul y otro color");
		assertEquals(juego.devolverPista(TipoLugar.historico),"El sospechoso compro un boleto para viajar en el TGV");
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso compro euros" );
	}
	
	@Test
	public void testJuegoInicializaCasoADificultadDificilEnParis(){
		Juego juego = new Juego ();		
		int casosPolicia = 20;
		Policia policia = new Policia("gonzalez", casosPolicia);
		Capital paris = new Capital("Paris", "Francia");
		Capital roma = new Capital("Roma","Italia");
		
		juego.agregarAgente(policia);
		juego.crearCaso();
		juego.ladronRobaObjetoEn(roma);
		juego.policiaViajaA(roma);
		juego.ladronViajaA(paris);
	
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso viajo a un pais que tiene la industria agro-alimentaria mas importante de Europa");
		assertEquals(juego.devolverPista(TipoLugar.historico),"El sospechoso escucho Mirroirs de Maurice Ravel y se planteo visitar el pais de origen del compositor");
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"El sospechoso viajo a un pais con una bandera que invertida en 4 formas , representa 4 paises" );
	}
	
	
	@Test
	public void testJuegoInicializaCasoADificultadFacilConViajeDeLadron(){
		
		Juego juego = new Juego ();		
		int casosPolicia = 0;
		Policia policia = new Policia("gonzalez", casosPolicia);
		Capital paris = new Capital("Paris", "Francia");
		Capital roma = new Capital("Roma","Italia");
		Capital buenosAires = new Capital("Buenos Aires", "Argentina");
		
		juego.agregarAgente(policia);
		juego.crearCaso();
		juego.ladronRobaObjetoEn(roma);
		juego.ladronViajaA(paris);
		juego.policiaViajaA(roma);
		juego.ladronViajaA(buenosAires);
		juego.policiaViajaA(paris);
		
		//juego.ladronViajaA(buenosAires);
		//juego.policiaViajaA(destino1); el policia tiene que estar en el Origen para ver las pistas que lo lleven al destino1
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"El sospechoso fue a un pais cuya bandera es azul, blanca con un sol en el medio");
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso decidio ir a comprar pesos a un pais del sur de sudamerica");
		assertEquals(juego.devolverPista(TipoLugar.historico),"El sospechoso fue al pais en el cual uno de sus proceres libero a muchos paises de sudamerica" );
	}
	
	@Test
	public void testJuegoInicializaCasoADificultadMediaConViajeDeLadron(){
		
		Juego juego = new Juego ();		
		int casosPolicia = 10;
		Policia policia = new Policia("gonzalez", casosPolicia);
		Capital paris = new Capital("Paris", "Francia");
		Capital roma = new Capital("Roma","Italia");
		Capital buenosAires = new Capital("Buenos Aires", "Argentina");
		
		juego.agregarAgente(policia);
		juego.crearCaso();
		juego.ladronRobaObjetoEn(roma);
		juego.ladronViajaA(paris);
		juego.policiaViajaA(roma);
		juego.ladronViajaA(buenosAires);
		juego.policiaViajaA(paris);
		
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"El sospechoso fue a un pais cuya bandera es azul y blanca");
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso fue a un pais cuya principal actividad economica es la ganaderia en sudamerica");
		assertEquals(juego.devolverPista(TipoLugar.historico),"El sospechoso se intereso por aprender el tango en el pais de su origen" );
	}
	
	@Test
	public void testJuegoInicializaCasoADificultadDificilConViajeDeLadron(){
		
		Juego juego = new Juego ();		
		int casosPolicia = 20;
		Policia policia = new Policia("gonzalez", casosPolicia);
		Capital paris = new Capital("Paris", "Francia");
		Capital roma = new Capital("Roma","Italia");
		Capital buenosAires = new Capital("Buenos Aires", "Argentina");
		
		juego.agregarAgente(policia);
		juego.crearCaso();
		juego.ladronRobaObjetoEn(roma);
		juego.ladronViajaA(paris);
		juego.policiaViajaA(roma);
		juego.ladronViajaA(buenosAires);
		juego.policiaViajaA(paris);
		
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso intento comprar dolares en ese pais y no lo logro");
		assertEquals(juego.devolverPista(TipoLugar.historico),"El sospechoso se intereso por un pais cuyo voto universal fue derogado en 1912");
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"El sospechoso fue a un pais  cuya bandera tiene azul" );
	}
	
	@Test
	public void testJuegoBuscaPoliciaNovatoEInstanciaUnLadron(){
		Juego juego = new Juego ();
		LectorPoliciaXml lectorPolicia = new LectorPoliciaXml();
		String nombrePolicia = "Mat";
		int casosPolicia = lectorPolicia.leerCasos(nombrePolicia);
		Policia policia = new Policia("Mat",casosPolicia);
		juego.agregarAgente(policia);
		juego.crearCaso();
		Ladron ladron = juego.devolverLadron();
		assertEquals(ladron.getClass(),LadronMedia.class);
	}
	
	@Test
	public void testJuegoBuscaPoliciaEInstanciaUnaCapital(){
		Juego juego = new Juego ();
		LectorPoliciaXml lectorPolicia = new LectorPoliciaXml();
		String nombrePolicia = "Mat";
		int casosPolicia = lectorPolicia.leerCasos(nombrePolicia);
		Policia policia = new Policia("Mat",casosPolicia);
		Capital roma = new Capital("Roma","Italia");
		
		juego.agregarAgente(policia);	
		juego.crearCaso();
		juego.ladronRobaObjetoEn(roma);
		juego.policiaViajaA(roma);
		assertEquals(juego.devolverNombreCapitalActual(),"Roma");
	}
	
	@Test
	public void testJuegoIniciaCasoBuscandoUnPoliciaNovato(){
		Juego juego = new Juego ();
		LectorPoliciaXml lectorPolicia = new LectorPoliciaXml();
		String nombrePolicia = "Mat";
		int casosPolicia = lectorPolicia.leerCasos(nombrePolicia);
		Policia policia = new Policia(nombrePolicia,casosPolicia);
		Capital roma = new Capital("Roma","Italia");
		Capital paris = new Capital("Paris", "Francia");
		
		juego.agregarAgente(policia);	
		juego.crearCaso();
		juego.ladronRobaObjetoEn(paris);
		juego.policiaViajaA(paris);
		juego.ladronViajaA(roma);
		
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso compro una moneda europea");
		
	}
	
	
	@Test
	public void testJuegoIniciaCasoBuscandoUnPoliciaDetective(){
		Juego juego = new Juego ();
		LectorPoliciaXml lectorPolicia = new LectorPoliciaXml();
		String nombrePolicia = "Franco";
		int casosPolicia = lectorPolicia.leerCasos(nombrePolicia);
		Policia policia = new Policia("Franco",casosPolicia);
		
		Capital roma = new Capital("Roma","Italia");
		Capital paris = new Capital("Paris", "Francia");
		
		juego.agregarAgente(policia);	
		juego.crearCaso();
		juego.ladronRobaObjetoEn(paris);
		juego.policiaViajaA(paris);
		juego.ladronViajaA(roma);
		
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso compro una moneda europea");
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"El sospechoso viajo en un vehiculo con una bandera de colores verde, blanco y otro color");
		assertEquals(juego.devolverPista(TipoLugar.historico),"El sospechoso buscaba mapas del mediterraneo");
	}
	
	@Test
	public void testJuegoBuscaPoliciaDificilEInstanciaUnaCapitalYDejaPistasEnLugares(){
		Juego juego = new Juego ();
		LectorPoliciaXml lectorPolicia = new LectorPoliciaXml();
		String nombrePolicia = "Felipe";
		int casosPolicia = lectorPolicia.leerCasos(nombrePolicia);
		Policia policia = new Policia("Felipe",casosPolicia);
		Capital roma = new Capital("Roma","Italia");
		Capital paris = new Capital("Paris", "Francia");
		
		juego.agregarAgente(policia);	
		juego.crearCaso();
		juego.ladronRobaObjetoEn(paris);
		juego.policiaViajaA(paris);
		juego.ladronViajaA(roma);
		
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso compro billetes de 500");
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"El sospechoso viajo en un vehiculo con una bandera de colores verde y dos colores mas");
		assertEquals(juego.devolverPista(TipoLugar.historico),"El sospechoso buscaba libros de latin");

	}
	
	@Test
	public void testJuegoPruebaLadronNoDebeViajarMasDeSuRangoFacil(){
		
		Juego juego = new Juego ();		
		int casosPolicia = 0;

		Policia policia = new Policia("gonzalez", casosPolicia);
		Capital roma = new Capital("Roma", "Italia");
		Capital buenosAires = new Capital("Buenos Aires", "Argentina");
		Capital paris = new Capital("Paris", "Francia");
		
		juego.agregarAgente(policia);
		juego.crearCaso();
		
		juego.ladronRobaObjetoEn(roma);
		
		juego.ladronViajaA(paris);
		juego.ladronViajaA(buenosAires);
		juego.ladronViajaA(paris);
		assertTrue(juego.ladronPuedeViajar());
		juego.ladronViajaA(buenosAires);
		assertFalse(juego.ladronPuedeViajar());
		
	}
	
	
	@Test
	public void testJuegoPruebaLadronNoDebeViajarMasDeSuRangoMedio(){
		
		Juego juego = new Juego ();		
		int casosPolicia = 10;

		Policia policia = new Policia("gonzalez", casosPolicia);
		Capital roma = new Capital("Roma", "Italia");
		Capital buenosAires = new Capital("Buenos Aires", "Argentina");
		Capital paris = new Capital("Paris", "Francia");
		
		juego.agregarAgente(policia);
		juego.crearCaso();
		
		
		juego.ladronRobaObjetoEn(roma);
		
		juego.ladronViajaA(paris);
		juego.ladronViajaA(buenosAires);
		juego.ladronViajaA(paris);
		juego.ladronViajaA(buenosAires);
		juego.ladronViajaA(paris);
		assertTrue(juego.ladronPuedeViajar());
		juego.ladronViajaA(buenosAires);
		assertFalse(juego.ladronPuedeViajar());
		
	}
	
	@Test
	public void testJuegoPruebaLadronNoDebeViajarMasDeSuRangoDificil(){
		
		Juego juego = new Juego ();		
		int casosPolicia = 20;
		Policia policia = new Policia("gonzalez", casosPolicia);
		Capital roma = new Capital("Roma", "Italia");
		Capital buenosAires = new Capital("Buenos Aires", "Argentina");
		Capital paris = new Capital("Paris", "Francia");
		
		juego.agregarAgente(policia);
		juego.crearCaso();
		
		juego.ladronRobaObjetoEn(roma);
		juego.ladronViajaA(paris);
		juego.ladronViajaA(buenosAires);
		juego.ladronViajaA(paris);
		juego.ladronViajaA(buenosAires);
		juego.ladronViajaA(paris);
		juego.ladronViajaA(buenosAires);
		assertTrue(juego.ladronPuedeViajar());
		juego.ladronViajaA(paris);
		assertFalse(juego.ladronPuedeViajar());
		
	}
	
	@Test
	public void testJuegoBuscaPoliciaFacilEInstanciaUnaCapitalYDejaPistasEnSuTercerViaje(){
		Juego juego = new Juego ();
		LectorPoliciaXml lectorPolicia = new LectorPoliciaXml();
		String nombrePolicia = "Mat";
		int casosPolicia = lectorPolicia.leerCasos(nombrePolicia);
		Policia policia = new Policia(nombrePolicia,casosPolicia);
		Capital buenosAires = new Capital("Buenos Aires", "Argentina");
		Capital paris = new Capital("Paris", "Francia");
		Capital roma = new Capital("Roma", "Italia");
		
		juego.agregarAgente(policia);
		juego.crearCaso();
		juego.ladronRobaObjetoEn(buenosAires);
		juego.policiaViajaA(buenosAires);
		juego.ladronViajaA(paris);
		
		juego.policiaViajaA(paris);
		juego.ladronViajaA(buenosAires);
		juego.policiaViajaA(buenosAires);
		juego.ladronViajaA(roma);
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso compro una moneda europea");
		
	}

	@Test
	public void testJuegoBuscaPoliciaMedioEInstanciaUnaCapitalYDejaPistasEnSuTercerViaje(){
		Juego juego = new Juego ();
		LectorPoliciaXml lectorPolicia = new LectorPoliciaXml();
		String nombrePolicia = "Franco";
		int casosPolicia = lectorPolicia.leerCasos(nombrePolicia);
		Policia policia = new Policia(nombrePolicia,casosPolicia);
		Capital buenosAires = new Capital("Buenos Aires", "Argentina");
		Capital paris = new Capital("Paris", "Francia");
		Capital roma = new Capital("Roma", "Italia");
		
		juego.agregarAgente(policia);
		juego.crearCaso();
		juego.ladronRobaObjetoEn(buenosAires);
		juego.policiaViajaA(buenosAires);
		juego.ladronViajaA(paris);
		
		juego.policiaViajaA(paris);
		juego.ladronViajaA(roma);
		juego.policiaViajaA(roma);
		juego.ladronViajaA(buenosAires);
		
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"El sospechoso fue a un pais cuya bandera es azul y blanca");
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso fue a un pais cuya principal actividad economica es la ganaderia en sudamerica");
		assertEquals(juego.devolverPista(TipoLugar.historico),"El sospechoso se intereso por aprender el tango en el pais de su origen" );
	}
	
	
	
	public void testJuegoBuscaPoliciaDificilEInstanciaUnaCapitalYDejaPistasEnSuTercerViaje(){
		Juego juego = new Juego ();
		LectorPoliciaXml lectorPolicia = new LectorPoliciaXml();
		String nombrePolicia = "Felipe";
		int casosPolicia = lectorPolicia.leerCasos(nombrePolicia);
		Policia policia = new Policia(nombrePolicia,casosPolicia);
		Capital buenosAires = new Capital("Buenos Aires", "Argentina");
		Capital paris = new Capital("Paris", "Francia");
		
		
		juego.agregarAgente(policia);
		juego.crearCaso();
		juego.ladronRobaObjetoEn(buenosAires);
		juego.policiaViajaA(buenosAires);
		juego.ladronViajaA(paris);
		
		juego.policiaViajaA(paris);
		juego.ladronViajaA(buenosAires);
		juego.policiaViajaA(buenosAires);
		juego.ladronViajaA(paris);
		
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso viajo a un pais que tiene la industria agro-alimentaria mas importante de Europa");
		assertEquals(juego.devolverPista(TipoLugar.historico),"El sospechoso escucho Mirroirs de Maurice Ravel y se planteo visitar el pais de origen del compositor");
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"El sospechoso viajo a un pais con una bandera que invertida en 4 formas , representa 4 paises" );
	}
	
	@Test
	public void testJuegoBuscaPoliciaDificilEInstanciaUnaCapitalYBuscoPistasVacias(){
		
		Juego juego = new Juego ();
		LectorPoliciaXml lectorPolicia = new LectorPoliciaXml();
		String nombrePolicia = "Felipe";
		int casosPolicia = lectorPolicia.leerCasos(nombrePolicia);
		Policia policia = new Policia(nombrePolicia,casosPolicia);
		Capital roma = new Capital("Roma", "Italia");
		Capital buenosAires = new Capital("Buenos Aires", "Argentina");
		
		juego.agregarAgente(policia);
		juego.crearCaso();
		juego.ladronRobaObjetoEn(roma);
		juego.policiaViajaA(buenosAires);

		assertEquals(juego.devolverPista(TipoLugar.economico),"No he visto a nadie sospechoso");
		assertEquals(juego.devolverPista(TipoLugar.historico),"No he visto a nadie sospechoso");
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"No he visto a nadie sospechoso" );
	}
	
	
	public void testJuegoBuscaPoliciaDificilESeEquivocaDeCapitalYVuelveAViajarALaCorrecta(){
		
		Juego juego = new Juego ();
		LectorPoliciaXml lectorPolicia = new LectorPoliciaXml();
		String nombrePolicia = "Felipe";
		int casosPolicia = lectorPolicia.leerCasos(nombrePolicia);
		Policia policia = new Policia(nombrePolicia,casosPolicia);
		Capital roma = new Capital("Roma", "Italia");
		Capital buenosAires = new Capital("Buenos Aires", "Argentina");
		
		juego.agregarAgente(policia);
		juego.crearCaso();
		juego.ladronRobaObjetoEn(buenosAires);
		juego.policiaViajaA(roma);
		juego.policiaViajaA(buenosAires);
		juego.ladronViajaA(roma);

		
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso compro billetes de 500");
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"El sospechoso viajo en un vehiculo con una bandera de colores verde y dos colores mas");
		assertEquals(juego.devolverPista(TipoLugar.historico),"El sospechoso buscaba libros de latin");
		assertEquals(juego.devolverPista(TipoLugar.rasgo).getClass(),String.class);
	}


	
	public void testSimulacionDeUnJuegoFacilConLadronViajandoAVariasCapitales(){
		
	///DECLARACION VARIABLES////
		Juego juego = new Juego ();
		LectorPoliciaXml lectorPolicia = new LectorPoliciaXml();
		String nombrePolicia = "Mat";
		int casosPolicia = lectorPolicia.leerCasos(nombrePolicia);
		Policia policia = new Policia(nombrePolicia,casosPolicia);
		Capital buenosAires = new Capital("Buenos Aires", "Argentina");
		Capital paris = new Capital("Paris", "Francia");
		Capital roma = new Capital("Roma", "Italia");
		//Capital moscu = new Capital("Bagdad", "Irak");
		
	///METODOS EL JUEGO////	
		
		juego.agregarAgente(policia);
		juego.crearCaso();
		juego.ladronRobaObjetoEn(buenosAires);
		juego.ladronViajaA(roma);
		juego.policiaViajaA(buenosAires);
		
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso compro una moneda europea");
		
	
		///PRIMER VIAJE LADRON///
		
		juego.policiaViajaA(roma);
		juego.ladronViajaA(paris);
		
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"El sospechoso viajo a un pais con una bandera roja, azul y blanca");
		assertEquals(juego.devolverPista(TipoLugar.historico),"El sospechoso buscaba recetas para preparar baguettes");
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso compro francos" );
	
		///SEGUNDO VIAJE LADRON///	
		
		juego.policiaViajaA(paris);
		juego.ladronViajaA(buenosAires);
		
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"El sospechoso fue a un pais cuya bandera es azul, blanca con un sol en el medio");
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso decidio ir a comprar pesos a un pais del sur de sudamerica");
		assertEquals(juego.devolverPista(TipoLugar.historico),"El sospechoso fue al pais en el cual uno de sus proceres libero a muchos paises de sudamerica" );
	
		//// TERCER VIAJE ///
		
		juego.policiaViajaA(buenosAires);
		juego.ladronViajaA(roma);
		
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso compro euros");
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"El sospechoso viajo en un vehiculo con una bandera de colores verde, blanco y rojo");
		assertEquals(juego.devolverPista(TipoLugar.historico),"El sospechoso buscaba libros de gladiadores");
		
		////CUARTO VIAJE////
		
		juego.policiaViajaA(roma);
		juego.ladronViajaA(paris);
		
		
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"El sospechoso viajo a un pais con una bandera roja, azul y blanca");
		assertEquals(juego.devolverPista(TipoLugar.historico),"El sospechoso buscaba recetas para preparar baguettes");
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso compro francos" );
	
		////QUINTO VIAJE////
		
		juego.policiaViajaA(paris);

		assertTrue( !(juego.ladronPuedeViajar()) );
		
	}
	

	@Test
	public void testLadronRobaObjetoAlAzar(){
		
	///DECLARACION VARIABLES////
		Juego juego = new Juego ();
		LectorPoliciaXml lectorPolicia = new LectorPoliciaXml();
		String nombrePolicia = "Mat";
		int casosPolicia = lectorPolicia.leerCasos(nombrePolicia);
		Policia policia = new Policia(nombrePolicia,casosPolicia);

		juego.agregarAgente(policia);
		juego.crearCaso();
		juego.ladronRobaObjetoEn();
		juego.policiaViajaA();
	
		assertEquals(juego.devolverObjetoRobado().getClass(),String.class);

	}
}