package fiuba.algo3.tp2.interfaces;

public interface Rangos {
	
	public Integer getVelocidadDesplazamiento();
	
	public String getNombreRango();
}
