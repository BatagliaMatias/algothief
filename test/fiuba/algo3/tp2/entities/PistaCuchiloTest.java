package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.interfaces.Pista;

public class PistaCuchiloTest {

	@Test
	public void testPistaCuchillo() {
		Pista pistaCuchillo = new PistaCuchillo();
		assertEquals(pistaCuchillo.getDescripcion(),"Has sido acuchillado");
	}
	
	@Test
	public void testPistaCuchilloPolicia() {
		Policia policia = new Policia("Mat",0);
		Pista pistaCuchillo = new PistaCuchillo();
		assertEquals(pistaCuchillo.interactuar(policia),"Has sido acuchillado");
		assertEquals(policia.getHora(),"Lunes 10:00");
	}

}
