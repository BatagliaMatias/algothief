package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;
import java.util.ArrayList;

import org.junit.Test;

public class LectorPoliciaXmlTest {

	@Test
	public void testLeerNivelPoliciaMat() {
		LectorPoliciaXml lector = new LectorPoliciaXml();
		String nombrePolicia = "PoliciaPrueba";
		int casosXml = lector.leerCasos(nombrePolicia);
		int casosEsperado = 0;
		assertEquals(casosXml, casosEsperado);
	}
	
	@Test
	public void testGetTodosLosPolicias(){
		LectorPoliciaXml lector = new LectorPoliciaXml();
		ArrayList<String> nombrePolicias = lector.getTodosLosPolicias();
		String Policia = "Franco";
		String Policia2 = "Felipe";
		assertEquals(Policia, nombrePolicias.get(2));
		assertEquals(Policia2, nombrePolicias.get(3));
		
	}

}
