package fiuba.algo3.tp2.factories;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.entities.LadronDificil;
import fiuba.algo3.tp2.interfaces.DificultadFactory;
import fiuba.algo3.tp2.interfaces.Ladron;


public class DificilFactoryTest {
/*
	@Test
	public void testFactoryCrearPistaDificil() {
		DificultadFactory factory = new DificilFactory();
		CreadorPista pistaFactory = factory.crearPista();
		assertEquals(pistaFactory.getClass(), CreadorPistaDificil.class);		
	}
	*/
	@Test
	public void testFactoryCrearLadronDificil(){
		DificultadFactory factory = new DificilFactory();
		Ladron ladronFactory = factory.crearLadron();
		assertEquals(ladronFactory.getClass(), LadronDificil.class);
	}

}
