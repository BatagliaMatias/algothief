package fiuba.algo3.tp2.entities;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LectorCiudadConectadas implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Map<String, Integer> leerConexiones(String ciudadPrincipal) {
		HashMap<String, Integer> conexiones = new HashMap<String, Integer>();
		Document document;
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.parse(new File("xml/capitalesConectadas.xml"));
			NodeList nodeList = document.getDocumentElement().getChildNodes();
			
			for (int i = 0; i < nodeList.getLength(); i++) {
               Node node = nodeList.item(i);
               if (node.getNodeType() == Node.ELEMENT_NODE) {
                  String nombreCapital1 = node.getAttributes().getNamedItem("NombreCapital1").getNodeValue();
                  String nombreCapital2 = node.getAttributes().getNamedItem("NombreCapital2").getNodeValue();
                  Integer distancia = Integer.parseInt(node.getAttributes().getNamedItem("Distancia").getNodeValue());
                  
                  if(nombreCapital1.equals(ciudadPrincipal)){
                	  conexiones.put(nombreCapital2, distancia);
                  }
                  else{
                	  if(nombreCapital2.equals(ciudadPrincipal)){
                		  conexiones.put(nombreCapital1,distancia);
                	  }
                  }
                  
               }
			}
          
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return conexiones;
	}

}
