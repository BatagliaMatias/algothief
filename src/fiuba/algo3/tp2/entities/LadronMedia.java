package fiuba.algo3.tp2.entities;

import java.io.Serializable;

import fiuba.algo3.tp2.interfaces.Ladron;
import fiuba.algo3.tp2.interfaces.Viajable;


public class LadronMedia extends LadronGenerico implements Ladron, Viajable , Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final int MAXVIAJES = 6;
	
	public LadronMedia(String nombre, String sexo, String hobby,
			String cabello, String senia, String vehiculo) {
		super(nombre, sexo, hobby, cabello, senia, vehiculo, "media");
		contadorViajesDisponibles = MAXVIAJES;
	}

	@Override
	public String objetoRobado() {
		return "auto";
	}

	@Override
	public int rutaEscape() {
		return 6;
	}
	public void setContadorViajesDisponibles(int contadorViajesDisponibles) {
		this.contadorViajesDisponibles = contadorViajesDisponibles;
	}

	public boolean viajeDisponible() {
		if (this.contadorViajesDisponibles>0) return true;
		else return false;
	}
	
}
