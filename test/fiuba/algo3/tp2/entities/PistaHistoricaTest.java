package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;


import fiuba.algo3.tp2.interfaces.Pista;

public class PistaHistoricaTest {

	@Test
	public void testPistaDificilBibloteca(){
		Pista pistaHistorica = new PistaHistorica();
		LectorPistaXml lector = new LectorPistaXml();
		String dificultad = "dificil";
		String tipo = "historico";
		String ciudad = "Roma";
		String esperado = "El sospechoso buscaba libros de latin";
		pistaHistorica.setDescripcion(lector.leerPista(ciudad, dificultad, tipo));
		assertEquals(pistaHistorica.getDescripcion(), esperado);
				
	}
	
	
	
	
}
