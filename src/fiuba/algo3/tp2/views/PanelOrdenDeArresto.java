package fiuba.algo3.tp2.views;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import fiuba.algo3.tp2.controllers.ControladorAccionesVisitarLugares;
import fiuba.algo3.tp2.entities.*;


public class PanelOrdenDeArresto extends JPanel implements Observer{

	private static final long serialVersionUID = 1L;
	
	private ControladorAccionesVisitarLugares controlador;
//	private Object controladorOrdenDeArresto;
	private Juego juego;
	private ArrayList<String> rasgos;
	
	public PanelOrdenDeArresto(Juego modelo){//, ControladorOrdenDeArresto controladorOrdenArresto){
		
//		this.controladorOrdenDeArresto = controladorOrdenArresto;
		this.juego = modelo;
		juego.addObserver(this);
		
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		rasgos = new ArrayList<String>();
		
		setVisible(true);
		JLabel titulo = new JLabel("Orden de arresto");
		this.add(titulo);
		setBackground(Color.LIGHT_GRAY);
		this.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		this.mostrar();
		
		
		
		
	}
		

	public void update(Observable o, Object arg) {
				this.mostrar();		
		
		
		
	}
	
	
	private void mostrar(){
		
		OrdenDeArresto orden = juego.getPolicia().getOrden();
		
		HashSet<Rasgo> rasgos = orden.getRasgos();
		
		this.removeAll();
		setBackground(Color.LIGHT_GRAY);
		this.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		JLabel titulo = new JLabel("Orden de arresto");
		this.add(titulo);
		Iterator<Rasgo> it = rasgos.iterator();
		while(it.hasNext()){
			
		
			String rasgo = (it.next().getDescripcion());
			agregarImagen("rasgos/"+rasgo+".png");
			
		}
		

		setVisible(true);
	
		this.validate();
		this.repaint();
		
		
	}
	
	
	private void agregarImagen(String ruta) {
		Imagen imagen = new Imagen(ruta);
		imagen.setVisible(true);
		this.add(imagen);
		
	}

	public void setJuego(Juego juegoActual) {
		juego = juegoActual;		
		juego.addObserver(this);
		juego.getPolicia().addObserver(this);
	
	}


	public void borrarRasgos() {
		
		ArrayList<String> rasgos = null;
		this.removeAll();
		setBackground(Color.LIGHT_GRAY);
		this.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		setVisible(true);
		
		this.validate();
		this.repaint();
	}
	

	
	

}
