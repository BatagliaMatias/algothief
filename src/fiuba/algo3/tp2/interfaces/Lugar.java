package fiuba.algo3.tp2.interfaces;

public interface Lugar  {
	public enum TipoLugar{economico,historico,descriptivo,rasgo}

	String devolverPista();
	
	Pista devolverPistaCompleta();

	String getDescripcionLugar();

	void setearPista(Pista pista);
	
	public TipoLugar getTipoLugar();
	
}
