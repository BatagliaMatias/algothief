package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;

public class EscenarioTest {

	@Test
	public void testCrearEscenarioObtenerItalia() {
		Escenario escenario = new Escenario();
		Capital capital = escenario.obtenerCapital("Roma");
		assertEquals(capital.getNombrePais(), "Italia");
	}
	
	@Test
	public void testCrearEscenarioPistaVacia() {
		Escenario escenario = new Escenario();
		Capital capital = escenario.obtenerCapital("Roma");
		assertEquals(capital.getDescripcionPista(TipoLugar.descriptivo), "No he visto a nadie sospechoso");
	}

}
