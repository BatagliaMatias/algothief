package fiuba.algo3.tp2.entities;

import java.io.Serializable;

import fiuba.algo3.tp2.interfaces.Lugar;
import fiuba.algo3.tp2.interfaces.Pista;

public class LugarDescriptivo implements Lugar , Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String descripcionLugar;
	private Pista pistaDescriptiva;
	
	public LugarDescriptivo(){
		descripcionLugar= "Aeropuerto";
	}

	public String getDescripcionLugar() {
		return descripcionLugar;
	}
	


	public void setDescripcionLugar(String descripcionLugar) {
		this.descripcionLugar = descripcionLugar;
	}
	public void setearPista(Pista pista) {
		this.pistaDescriptiva = pista;
	}
	
	public String devolverPista(){
		return pistaDescriptiva.getDescripcion();
	}

	public TipoLugar getTipoLugar() {
		return TipoLugar.descriptivo;
	}

	
	public Pista devolverPistaCompleta() {
		return pistaDescriptiva;
	}
	
	

}
