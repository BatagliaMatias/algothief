package fiuba.algo3.tp2.entities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;

public class Persistencia implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public void guardarArchivo(Juego juego){
		
		try {
			File folder = new File("saved");
			if (!folder.exists()) { 
				folder.mkdir();
			}
			
			OutputStream fos = new FileOutputStream("saved/"+juego.getPolicia().getNombre()+".dat");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(juego);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public Juego cargarAchivo(String nombrePolicia){
		try {
			InputStream fis = new FileInputStream("saved/"+nombrePolicia+".dat");
			ObjectInputStream ois = new ObjectInputStream(fis);
			Juego objeto = (Juego)ois.readObject();
			return objeto;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
}
