package fiuba.algo3.tp2.factories;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Random;

import fiuba.algo3.tp2.entities.LadronMedia;
import fiuba.algo3.tp2.entities.LectorLadronXml;
import fiuba.algo3.tp2.interfaces.DificultadFactory;
import fiuba.algo3.tp2.interfaces.Ladron;


public class MediaFactory implements DificultadFactory , Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Ladron crearLadron() {
		LectorLadronXml lectorLadron =new LectorLadronXml();
		//Aca tendria que haber un random para que eliga un numero del 1-9, el 10 es carmen
		int numeroLadron =randInt(1,9) ;
		HashMap<String, String> ladronDatos = lectorLadron.obtenerLadron(numeroLadron);

		
		return new LadronMedia(ladronDatos.get("nombre"),ladronDatos.get("sexo"), ladronDatos.get("hobby"),ladronDatos.get("cabello"),ladronDatos.get("senia"),ladronDatos.get("vehiculo"));
	}
	
	public static int randInt(int min, int max) {

	    // Usually this should be a field rather than a method variable so
	    // that it is not re-seeded every call.
	    Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
}
