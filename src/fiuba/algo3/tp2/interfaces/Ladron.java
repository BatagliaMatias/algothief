package fiuba.algo3.tp2.interfaces;

import fiuba.algo3.tp2.entities.Capital;
import fiuba.algo3.tp2.entities.LadronGenerico.TipoRasgo;
import fiuba.algo3.tp2.entities.Rasgo;


public interface Ladron {

	String objetoRobado();

	int rutaEscape();
	
	Capital getUbicacion();
	
	void dejarPista(Capital ubicacion, Capital Destino);
	
	void dejarTrampasYEsconderse(Capital capital);

	String devolverNombreLadron();

	Rasgo getRasgo(TipoRasgo tiporasgo);
	
	boolean contieneRasgo(Rasgo rasgo);

	boolean viajeDisponible();

	void restarContador();

	boolean esCapitalInicial();
	
	String determinarNombreProximaCiudad();

}