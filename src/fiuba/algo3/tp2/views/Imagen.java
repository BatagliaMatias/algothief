package fiuba.algo3.tp2.views;

import java.awt.Color;

import java.awt.FlowLayout;
import java.awt.Graphics;

import javax.swing.JLabel;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Imagen extends JPanel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BufferedImage image;

    public Imagen(String ruta) {
       try {  
    	   String rutaOK = ruta.replaceAll(" ", "");
    	  
    	   setLayout(new FlowLayout(FlowLayout.CENTER));
    	   setBackground(Color.LIGHT_GRAY);
    	   setAlignmentX(CENTER_ALIGNMENT);
          image = ImageIO.read(new File(rutaOK));
          this.setSize(image.getWidth(), image.getHeight());
          this.setBounds(0, 0, image.getWidth(), image.getHeight());
         
          
       } catch (IOException ex) {
           ex.printStackTrace();
       }
    }
    
    public Imagen(String ruta,String texto) {
        try {  
     	   String rutaOK = ruta.replaceAll(" ", "");
     	   JLabel label = new JLabel(texto);
     	   setLayout(new FlowLayout(FlowLayout.CENTER));
     	   setBackground(Color.LIGHT_GRAY);
     	   setAlignmentX(CENTER_ALIGNMENT);
           image = ImageIO.read(new File(rutaOK));
           this.setSize(image.getWidth(), image.getHeight());
           this.setBounds(0, 0, image.getWidth(), image.getHeight());
           this.add(label);
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
     }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null); // see javadoc for more info on the parameters            
    }

}