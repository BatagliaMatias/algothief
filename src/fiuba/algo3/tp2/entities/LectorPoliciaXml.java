package fiuba.algo3.tp2.entities;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import java.util.ArrayList;

public class LectorPoliciaXml implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final int NO_ENCONTRADO = -1;

	public int leerCasos(String nombrePolicia) {
		
		Document document;
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.parse(new File("xml/policias.xml"));
			NodeList nodeList = document.getDocumentElement().getChildNodes();
			
			for (int i = 0; i < nodeList.getLength(); i++) {
               Node node = nodeList.item(i);
               if (node.getNodeType() == Node.ELEMENT_NODE) {
                  String nombreXML = node.getAttributes().getNamedItem("nombre").getNodeValue();                    
                    
                    if (nombreXML.equals(nombrePolicia)){
                    	int casosXML = Integer.parseInt(node.getAttributes().getNamedItem("casos").getNodeValue());
                    	return casosXML;
                    }
               }
			}
          
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return NO_ENCONTRADO;
	}
	
	
	
public ArrayList<String> getTodosLosPolicias() {
		
		Document document;
		ArrayList<String> nombrePolicias = new ArrayList<String>();
		ArrayList<String> vacio = new ArrayList<String>();
		vacio.add("No hay policias");
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.parse(new File("xml/policias.xml"));
			NodeList nodeList = document.getDocumentElement().getChildNodes();
			
			for (int i = 0; i < nodeList.getLength(); i++) {
               Node node = nodeList.item(i);
               if (node.getNodeType() == Node.ELEMENT_NODE) {
                  String nombreXML = node.getAttributes().getNamedItem("nombre").getNodeValue();                    
                  nombrePolicias.add(nombreXML);
               }
			}
			return nombrePolicias;
          
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return vacio;
	
	}
	

	
}
