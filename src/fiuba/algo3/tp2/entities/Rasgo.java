package fiuba.algo3.tp2.entities;

import java.io.Serializable;

public class Rasgo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String descripcion;
	
	public Rasgo(String descripcion){
		this.descripcion = descripcion;
	}
	
	public String getDescripcion(){
		return descripcion;
	}
}
