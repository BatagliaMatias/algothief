package fiuba.algo3.tp2.exceptions;

public class ExcepcionCapitalesNoConectadas extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ExcepcionCapitalesNoConectadas() {
		super("Las Capitales no estan conectadas");
    }


}
