package fiuba.algo3.tp2.entities;

import java.io.Serializable;

import fiuba.algo3.tp2.interfaces.Pista;

public class PistaLadronFinal implements Pista , Serializable { //Se puede dejar cuando el ladron ya no tiene mas escape asi el policia interactua y ve si lo atrapa

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public String interactuar(Policia policia) {
		policia.pierdeTiempo(1); //entrar edificio
		if(policia.verificarOrdenDeArresto()){
			policia.arrestoConseguido();
			return "Ladron Atrapado";
		}
		else{
			return "Has encontrado al ladron, pero la orden de arresto no es valida";
		}
		
	}


	public void setDescripcion(Pista pista) {
		// TODO Auto-generated method stub

	}


	public String getDescripcion() {
		return "Has encontrado al Ladron";
	}


}
