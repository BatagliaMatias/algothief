package fiuba.algo3.tp2.views;

import java.awt.Color;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fiuba.algo3.tp2.entities.Juego;
import fiuba.algo3.tp2.entities.LectorPoliciaXml;

public class PanelTiempoUbicacion  extends JPanel implements Observer{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Juego juego;
	private JLabel salida;
	private LectorPoliciaXml lector;
	public PanelTiempoUbicacion(Juego juego){
		this.juego = juego;
		lector = new LectorPoliciaXml();
		String policia = juego.getPolicia().getNombre();
		Integer casos = lector.leerCasos(policia);
		String strCasos = casos.toString();
		String objeto = juego.devolverObjetoRobado();
		
		String rango = juego.getPolicia().rango();
		String tiempo = juego.getPolicia().getHora();	
		String horasRestantes = juego.getPolicia().tiempoRestante().toString();
		String ubicacion = juego.getPolicia().getUbicacion().getNombreCapital()+" || " + juego.getPolicia().getUbicacion().getNombrePais();
		String salidaString = rango +" "+policia +" con "+ strCasos+ " casos resueltos || "+tiempo + " || " + ubicacion + " || Horas disponibles: "+ horasRestantes + " || Buscando: "+objeto;
		salida = new JLabel(salidaString);
		salida.setVisible(true);
		this.add(salida);
		setVisible(true);
		setBackground(Color.LIGHT_GRAY);
		juego.addObserver(this);
		juego.getPolicia().addObserver(this);
		this.validate();
		this.repaint();
		
	}
	
	
	public void update(Observable o, Object arg) {
		String objeto = juego.devolverObjetoRobado();
		String policia = juego.getPolicia().getNombre();
		Integer casos = lector.leerCasos(policia);
		String strCasos = casos.toString();
		String rango = juego.getPolicia().rango();
		String tiempo = juego.getPolicia().getHora();
	/*	if (tiempo == "Fin del tiempo") 
		{
			JOptionPane.showMessageDialog(this, "Te has quedado sin tiempo!!");	
			//VentanaPrincipal.
			//AplicacionVisual.iniciar();
			
		}*/
		//else {	
		String horasRestantes = juego.getPolicia().tiempoRestante().toString();
		String ubicacion = juego.getPolicia().getUbicacion().getNombreCapital()+" || " + juego.getPolicia().getUbicacion().getNombrePais();
		String salidaString = rango +" "+policia +" con "+ strCasos+ " casos resueltos || "+tiempo + " || " + ubicacion + " || Horas disponibles: "+ horasRestantes + " || Buscando: "+objeto;
		this.remove(salida);
		salida = new JLabel(salidaString);
		this.add(salida);
		salida.setVisible(true);
		this.validate();
		this.repaint();
	//	}
		
	}

	public void setJuego(Juego juegoActual) {
		juego = juegoActual;		
		juego.addObserver(this);
		juego.getPolicia().addObserver(this);
		
	}
	
	

	
}
