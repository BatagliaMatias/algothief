package fiuba.algo3.tp2.entities;


import java.io.Serializable;

import fiuba.algo3.tp2.interfaces.Ladron;
import fiuba.algo3.tp2.interfaces.Viajable;


public class LadronFacil extends LadronGenerico implements Ladron, Viajable , Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final int MAXVIAJES = 4;
	
	public LadronFacil(String nombre, String sexo, String hobby,
			String cabello, String senia, String vehiculo) {
		super(nombre, sexo, hobby, cabello, senia, vehiculo,"facil");
		this.contadorViajesDisponibles = MAXVIAJES;
	}

	@Override
	public String objetoRobado() {
		return "celular";
	}

	@Override
	public int rutaEscape() {
		return MAXVIAJES;
	}

	public void setContadorViajesDisponibles(int contadorViajesDisponibles) {
		this.contadorViajesDisponibles = contadorViajesDisponibles;
	}

	public boolean viajeDisponible() {
		if (contadorViajesDisponibles > 0) return true;
		else return false;
	}
	
}
