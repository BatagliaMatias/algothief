package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

public class ModificadorPoliciaXmlTest {

	@Test
	public void testModificarXml() {
		ModificadorPoliciaXml modificador = new ModificadorPoliciaXml();
		LectorPoliciaXml lector = new LectorPoliciaXml();
		
		String nombrePolicia = "PoliciaPrueba";
		int casosPedidos = 0;
		modificador.actualizar(nombrePolicia,casosPedidos);
		
		int casos = lector.leerCasos(nombrePolicia);
		
		assertEquals(casos,casosPedidos);
		
	}

}
