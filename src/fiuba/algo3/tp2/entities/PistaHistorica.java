package fiuba.algo3.tp2.entities;

import java.io.Serializable;

import fiuba.algo3.tp2.interfaces.Pista;

public class PistaHistorica implements Pista , Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String descripcion;

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;

	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(Pista pista) {
		this.descripcion = pista.getDescripcion();
	}
	

	public String interactuar(Policia policia) {
		policia.pierdeTiempo(1); //entrar edificio
		return this.getDescripcion();
		
	}


}
