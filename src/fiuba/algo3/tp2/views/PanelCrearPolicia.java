package fiuba.algo3.tp2.views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fiuba.algo3.tp2.entities.ModificadorPoliciaXml;

public class PanelCrearPolicia extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static JButton inputButton = new JButton("Nuevo Policia");
	public static JTextField editTextArea = new JTextField();
	private VentanaPrincipal ventanaPrincipal;
	
	public PanelCrearPolicia(final VentanaPrincipal ventanaPrincipal){
		this.ventanaPrincipal = ventanaPrincipal;
	    setLayout(new FlowLayout(FlowLayout.CENTER,1000,0));
	    
		JLabel label = new JLabel("Crear Policia");
		Font labelFont = label.getFont();
		label.setFont(new Font(labelFont.getName(), Font.PLAIN, 30));
		label.setAlignmentX(CENTER_ALIGNMENT);
		JLabel vacio1 = new JLabel("            ");
		JLabel vacio2 = new JLabel("            ");
		JLabel vacio3 = new JLabel("            ");
		this.add(label);
		
		final JLabel l=new JLabel();
		l.setText("Ingrese su nombre si es nuevo en la Academia");
		
		
		
		
		editTextArea.setPreferredSize(new Dimension(200,30));
		inputButton.setPreferredSize(new Dimension(200,30));
		
		add(vacio1);
		add(vacio2);
		add(l);
		add(editTextArea);
		add(vacio3);
	    add(inputButton);

	    																																										
	    inputButton.addActionListener(new ActionListener(){ public void actionPerformed( ActionEvent e){ String salida = editTextArea.getText().trim();
	    																								ModificadorPoliciaXml modificador = new ModificadorPoliciaXml();
	    																								modificador.actualizar(salida,0);
	    																								ventanaPrincipal.policiaCreado(salida);
	    																								} } );
	    
		//setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		setSize(400,400);
		setVisible(true);
		setBackground(Color.LIGHT_GRAY);
	    
	    
	}
	
	
	
}
