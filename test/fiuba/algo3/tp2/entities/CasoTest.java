package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.entities.LadronGenerico.TipoRasgo;
import fiuba.algo3.tp2.entities.Policia;
import fiuba.algo3.tp2.factories.CasoFactory;
import fiuba.algo3.tp2.interfaces.Ladron;
import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;

public class CasoTest {
	
	Caso caso;
	CasoFactory factoria;

	@Test
	public void testCasoNovatoConCapitalParis() {
		
		factoria = new CasoFactory();
		Policia policia = new Policia("Felipe", 0);
		Capital capital = new Capital("Paris","Francia");
		
		caso = factoria.crearCasoRango(policia);
		caso.crearLadron();
		caso.setearCapital(capital);
		
		assertEquals(caso.getCapitalNombre(),"Paris");
		assertEquals(caso.getCapitalPais(),"Francia");
	}
	
	@Test
	public void testCasoDevolverDescripcionDeLugares() {
		
		factoria = new CasoFactory();
		Policia policia = new Policia("Felipe", 0);
		Capital capital = new Capital("Paris","Francia");
		
		caso = factoria.crearCasoRango(policia);
		caso.crearLadron();
		caso.setearCapital(capital);
	
		assertEquals(caso.getDescripcionLugar(TipoLugar.economico),"Bolsa de Valores");
		assertEquals(caso.getDescripcionLugar(TipoLugar.historico),"Museo");
		assertEquals(caso.getDescripcionLugar(TipoLugar.descriptivo),"Aeropuerto");
		
	}
	
	@Test
	public void testCasoDevolverDescripcionDeLadron() {
		
		factoria = new CasoFactory();
		Policia policia = new Policia("Felipe", 0);
		Capital capital = new Capital("Paris","Francia");
		
		caso = factoria.crearCasoRango(policia);
		caso.crearLadron();
		caso.setearCapital(capital);
		Ladron ladron = caso.devolverLadron();
		
		assertEquals(caso.getRasgoLadron(TipoRasgo.cabello), ladron.getRasgo(TipoRasgo.cabello).getDescripcion());
		assertEquals(caso.getNombreLadron(),ladron.devolverNombreLadron());
	}
	
	
	@Test
	public void testCasoDevolverPistaDejadaPorLadronFacil() {
		
		factoria = new CasoFactory();
		Policia policia = new Policia("Felipe", 0);
		Capital roma = new Capital("Roma","Italia");
		Capital paris = new Capital("Paris","Francia");
		caso = factoria.crearCasoRango(policia);
		
		caso.crearLadron();
		caso.setearUbicacionLadron(paris);
		caso.setearUbicacionPoliciaEnCaso(paris);
		caso.ladronViaja(roma);
		
		assertEquals(caso.devolverDescripcionPista(TipoLugar.economico),"El sospechoso compro euros");
		assertEquals(caso.devolverDescripcionPista(TipoLugar.historico),"El sospechoso buscaba libros de gladiadores");
		assertEquals(caso.devolverDescripcionPista(TipoLugar.descriptivo),"El sospechoso viajo en un vehiculo con una bandera de colores verde, blanco y rojo");
	}
	
	@Test
	public void testCasoDevolverPistaDejadaPorLadronMedio() {
		
		factoria = new CasoFactory();
		Policia policia = new Policia("Felipe", 0);
		Capital roma = new Capital("Roma","Italia");
		Capital paris = new Capital("Paris","Francia");
		policia.subirARangoInvestigador();
		
		caso = factoria.crearCasoRango(policia);
		caso.crearLadron();
		caso.setearUbicacionLadron(paris);
		caso.setearUbicacionPoliciaEnCaso(paris);
		caso.ladronViaja(roma);
		
		assertEquals(caso.devolverDescripcionPista(TipoLugar.economico),"El sospechoso compro una moneda europea");
		assertEquals(caso.devolverDescripcionPista(TipoLugar.historico),"El sospechoso buscaba mapas del mediterraneo");
		assertEquals(caso.devolverDescripcionPista(TipoLugar.descriptivo),"El sospechoso viajo en un vehiculo con una bandera de colores verde, blanco y otro color");
	}
	
	@Test
	public void testCasoDevolverPistaDejadaPorLadronDificil() {
		
		factoria = new CasoFactory();
		Policia policia = new Policia("Felipe", 0);
		Capital roma = new Capital("Roma","Italia");
		Capital paris = new Capital("Paris","Francia");
		policia.subirARangoSargento();
		
		caso = factoria.crearCasoRango(policia);
		caso.crearLadron();
		caso.setearUbicacionLadron(paris);
		caso.setearUbicacionPoliciaEnCaso(paris);
		caso.ladronViaja(roma);
		
		assertEquals(caso.devolverDescripcionPista(TipoLugar.economico),"El sospechoso compro billetes de 500");
		assertEquals(caso.devolverDescripcionPista(TipoLugar.historico),"El sospechoso buscaba libros de latin");
		assertEquals(caso.devolverDescripcionPista(TipoLugar.descriptivo),"El sospechoso viajo en un vehiculo con una bandera de colores verde y dos colores mas");
	}
	
	
}
