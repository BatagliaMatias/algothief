package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.exceptions.ExcepcionCapitalesNoConectadas;
import fiuba.algo3.tp2.interfaces.Ladron;

public class LadronMediaTest {

	@Test
	public void testIdentificarObjetoParaRobar() {
		Ladron ladronMedia = new LadronMedia(null, null, null, null, null, null);
		String botin = "auto";
		assertEquals(ladronMedia.objetoRobado(), botin);
		
	}
	
	@Test
	public void testRutaDeEscape(){
		Ladron ladronMedia = new LadronMedia(null, null, null, null, null, null);
		int cantidadCapitales = 6;
		assertEquals(ladronMedia.rutaEscape(), cantidadCapitales);
	}
	
	@Test
	public void testViajesDisponibleMedia(){
		LadronGenerico ladron = new LadronFacil(null, null, null, null, null, null);
		assertTrue(ladron.viajeDisponible());
		
		Capital capital = new Capital("Moscu","Paris");
		Capital destino = new Capital("Roma","Italia");
		for(int i=0;i<6;i++){
		ladron.setUbicacion(capital);
		try {
			ladron.viajar(destino);
		} catch (ExcepcionCapitalesNoConectadas e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ladron.restarContador();
		}
				
		assertFalse(ladron.viajeDisponible()); 
		
	}
	
}
