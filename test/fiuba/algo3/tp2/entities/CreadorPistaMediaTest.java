package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.interfaces.CreadorPista;
import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;

public class CreadorPistaMediaTest {

	@Test
	public void testPistaMediaEconomica(){
		CreadorPista pistaMedia = new CreadorLadronMedia();
		String ciudad = "Roma";
		String esperado = "El sospechoso compro una moneda europea";
		assertEquals(pistaMedia.darPista(ciudad,TipoLugar.economico), esperado);
				
	}
	
	@Test
	public void testPistaMediaPuerto(){
		CreadorPista pistaMedia = new CreadorLadronMedia();
		String ciudad = "Roma";
		String esperado = "El sospechoso viajo en un vehiculo con una bandera de colores verde, blanco y otro color";
		assertEquals(pistaMedia.darPista(ciudad,TipoLugar.descriptivo),esperado);
	}
	
	@Test
	public void testPistaMediaBibloteca(){
		CreadorPista pistaMedia = new CreadorLadronMedia();
		String ciudad = "Roma";
		String esperado = "El sospechoso buscaba mapas del mediterraneo";
		assertEquals(pistaMedia.darPista(ciudad,TipoLugar.historico), esperado);
	}

}
