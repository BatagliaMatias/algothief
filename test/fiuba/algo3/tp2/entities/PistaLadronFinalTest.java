package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.factories.FacilFactory;
import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;

public class PistaLadronFinalTest {

	
	public void testLadronEncontrado() {
		Caso caso = new Caso(new FacilFactory());
		Policia policia = new Policia("Mat", 0);
		caso.ladronViaja(caso.getEscenario().obtenerCapital("Paris"));
		caso.ladronViaja(caso.getEscenario().obtenerCapital("Roma"));
		caso.ladronViaja(caso.getEscenario().obtenerCapital("Paris"));
		caso.ladronViaja(caso.getEscenario().obtenerCapital("Roma"));
		policia.setUbicacion(caso.getEscenario().obtenerCapital("Roma"));
		String salida = caso.getEscenario().obtenerCapital("Roma").getPista(TipoLugar.rasgo).interactuar(policia);
		System.out.println(salida);
		
	}
	@Test
	public void testParaCompilar(){
		assertEquals(1, 1);
	}
	
}
