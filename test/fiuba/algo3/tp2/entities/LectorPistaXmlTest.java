package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

public class LectorPistaXmlTest {

	@Test
	public void testLeerPistaEconomicaFacilRoma() {
		LectorPistaXml lector = new LectorPistaXml();
		String tipo = "economico";
		String dificultad = "facil";
		String ciudad = "Roma";
		assertEquals(lector.leerPista(ciudad,dificultad,tipo).getDescripcion(), "El sospechoso compro euros");
	}
	
	/*
	@Test
	public void testLeerPistaNoExiste(){
		LectorPistaXml lector = new LectorPistaXml();
		String tipo = "economico";
		String dificultad = "facil";
		String ciudad = "Marte";
		assertEquals(lector.leerPista(ciudad,dificultad,tipo).getDescripcion(), null);
	}
	*/
	/*
	@Test
	public void testLeerPistaPuertoFacilRoma(){
		LectorPistaXml lector = new LectorPistaXml();
		String tipo = "puerto";
		String dificultad = "facil";
		String ciudad = "Roma";
		String resultado = "El sospechoso viajo en un vehiculo con una bandera de colores verde, blanco y rojo";
		assertEquals(lector.leerPista(ciudad,dificultad,tipo),resultado);
	}
	
	@Test
	public void testLeerPistaBiblotecaFacilRoma(){
		LectorPistaXml lector = new LectorPistaXml();
		String tipo = "bibloteca";
		String dificultad = "facil";
		String ciudad = "Roma";
		String resultado = "El sospechoso buscaba libros de gladiadores";
		assertEquals(lector.leerPista(ciudad,dificultad,tipo),resultado);
	}
	*/

}
