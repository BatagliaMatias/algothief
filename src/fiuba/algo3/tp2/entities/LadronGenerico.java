package fiuba.algo3.tp2.entities;


//import java.util.ArrayList;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import fiuba.algo3.tp2.exceptions.ExcepcionCapitalesNoConectadas;
import fiuba.algo3.tp2.interfaces.Ladron;
//import fiuba.algo3.tp2.interfaces.Lugar;
import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;
import fiuba.algo3.tp2.interfaces.Pista;
import fiuba.algo3.tp2.interfaces.Viajable;



public class LadronGenerico implements Viajable, Ladron , Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Capital ubicacion; //Podria inicializarse con la capital donde se robo el objeto
	//private ArrayList<Rasgo> rasgos;	
	/*private String sexo;
	private String hobby;
	private String senia;
	private String cabello;
	private String vehiculo;
	*/private String nombre;
	private String dificultad;
	public int contadorViajesDisponibles;
	private LectorPistaXml lectorPista; 
	
	private Map<TipoRasgo, Rasgo> rasgos = new HashMap<TipoRasgo,Rasgo>();

	
	public enum TipoRasgo{sexo,hobby,cabello,senia,vehiculo,dificultad}


	public LadronGenerico(String nombre,String sexo, String hobby, String cabello, String senia,String vehiculo,String dificultad ) {
		
		this.nombre = nombre;
		/*this.sexo = sexo;
		this.hobby = hobby;
		this.cabello = cabello;
		this.senia = senia;
		this.vehiculo = vehiculo;
		*/
		this.dificultad = dificultad;
		
		rasgos.put(TipoRasgo.sexo, new Rasgo(sexo));
		rasgos.put(TipoRasgo.hobby, new Rasgo(hobby));
		rasgos.put(TipoRasgo.cabello, new Rasgo(cabello));
		rasgos.put(TipoRasgo.senia, new Rasgo(senia));
		rasgos.put(TipoRasgo.vehiculo, new Rasgo(vehiculo));
		//rasgos.put(TipoRasgo.dificultad, new Rasgo(dificultad));
		/*
		rasgos = new ArrayList<Rasgo>();
		rasgos.add(new Rasgo(sexo));
		rasgos.add(new Rasgo(hobby));
		rasgos.add(new Rasgo(cabello));
		rasgos.add(new Rasgo(senia));
		rasgos.add(new Rasgo(vehiculo));
		rasgos.add(new Rasgo(dificultad));
*/
		lectorPista = new LectorPistaXml();
	}
	
	public void setUbicacion(Capital capital){
		ubicacion = capital;
	}
	
	public Capital getUbicacion(){
		return ubicacion;
	}
	
	public Map<TipoRasgo, Rasgo> getRasgos(){
		return rasgos;
	}
	
	public String devolverNombreLadron() {
		return nombre;
	}

	public String objetoRobado() {
		return null;
	}

	public int rutaEscape() {
		return 0;
	}
	
	public String determinarNombreProximaCiudad(){
		
		Random random = new Random();
		List<String> keys = new ArrayList<String>(ubicacion.getConexiones().keySet());
		String nombreCiudadRandom = keys.get( random.nextInt(keys.size()) );
		return nombreCiudadRandom;
		
	}
	
	public void dejarPista(Capital capital, Capital destino){
		capital.getLugar(TipoLugar.economico).setearPista(lectorPista.leerPista(destino.getNombreCapital(), dificultad, TipoLugar.economico.toString()));
		capital.getLugar(TipoLugar.descriptivo).setearPista(lectorPista.leerPista(destino.getNombreCapital(), dificultad, TipoLugar.descriptivo.toString()));
		capital.getLugar(TipoLugar.historico).setearPista(lectorPista.leerPista(destino.getNombreCapital(), dificultad, TipoLugar.historico.toString()));
		capital.getLugar(TipoLugar.rasgo).setearPista(generarPistaRasgo());
	}
	
	public void dejarTrampasYEsconderse(Capital capital){
		ArrayList<TipoLugar> lugaresPosibles = new ArrayList<TipoLugar>();
		lugaresPosibles.add(TipoLugar.descriptivo);
		lugaresPosibles.add(TipoLugar.economico);
		lugaresPosibles.add(TipoLugar.historico);
		lugaresPosibles.add(TipoLugar.rasgo);
		
		long seed = System.nanoTime();
		Collections.shuffle(lugaresPosibles, new Random(seed));
		
		capital.getLugar(lugaresPosibles.get(0)).setearPista(new PistaCuchillo());
		capital.getLugar(lugaresPosibles.get(2)).setearPista(new PistaCuchillo());
		capital.getLugar(lugaresPosibles.get(1)).setearPista(new PistaArmaDeFuego());
		esconderse(capital,lugaresPosibles.get(3));
	}
	
	private void esconderse(Capital capital, TipoLugar tipoLugar) {
		capital.getLugar(tipoLugar).setearPista(new PistaLadronFinal());
	}

	private Pista generarPistaRasgo() {
		if(rasgos.isEmpty()){
			return new PistaCuchillo();
		}
		
		PistaRasgo pista = new PistaRasgo();
		
		Iterator<Entry<TipoRasgo, Rasgo>> it = rasgos.entrySet().iterator();		
		while (it.hasNext()) {
		 Entry<TipoRasgo, Rasgo> e = it.next();
		 pista.setearRasgo(e.getValue());
		 pista.setDescripcion(e.getValue().getDescripcion());
		 rasgos.remove(e.getKey());
		 return pista;
		}
		return null;
		
	}

	public void viajar(Capital destino) throws ExcepcionCapitalesNoConectadas {
				
		
		if (ubicacion.conexionExiste(destino.getNombreCapital())){
			this.dejarPista(ubicacion,destino);
			ubicacion = destino;			
		}
		
		else{
			 throw new ExcepcionCapitalesNoConectadas();
		}
		
	}
	/*
	public Rasgo getRasgo(int i) {
		return rasgos.get(i);
	}
*/
	public Rasgo getRasgo(TipoRasgo tiporasgo) {
		return rasgos.get(tiporasgo);
	}
	
	public boolean contieneRasgo(Rasgo rasgo){
		return rasgos.containsValue(rasgo);
	}

	public int getContadorViajesDisponibles() {
		return contadorViajesDisponibles;
	}

	public boolean viajeDisponible() {
		return contadorViajesDisponibles > 0;
	}

	public void restarContador() {
		contadorViajesDisponibles = contadorViajesDisponibles-1 ;	
	}
	
	//public void dejarPistaEnSuUbicacion(){
	//	this.dejarPista(ubicacion);
	//}

	public boolean esCapitalInicial() {
		if (ubicacion == null) return true; 
		else return false;
	}

	

}
