package fiuba.algo3.tp2.entities;

import java.io.Serializable;

import fiuba.algo3.tp2.interfaces.Rangos;

public final class Detective implements Rangos , Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final Integer velocidadDesplazamiento = 1100;
	final String nombreRango = "Detective";

	public Integer getVelocidadDesplazamiento() {
		return velocidadDesplazamiento;
	}
	
	public String getNombreRango() {
		return nombreRango;
	}
}
