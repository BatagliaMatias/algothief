package fiuba.algo3.tp2.views;
import java.awt.Color;


import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import fiuba.algo3.tp2.controllers.ControladorAccionesPrincipales;


public class PanelAccionesPrincipales extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private ControladorAccionesPrincipales controlador;
	
	public PanelAccionesPrincipales(ControladorAccionesPrincipales controlador){
			
		this.controlador = controlador;
		setVisible(true);
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		setBackground(Color.LIGHT_GRAY);
		
		JButton botonViajar = crearBoton("Viajar");
		JButton botonConexiones = crearBoton("Capitales Conectadas");
		JButton botonVisitarLugar = crearBoton("Visitar Lugar");
		JButton botonCerrarCaso = crearBoton("Cerrar Caso");
		JButton botonGuardar = crearBoton("Guardar");
		
		this.add(botonViajar);
		this.add(botonConexiones);
		this.add(botonVisitarLugar);
		this.add(botonCerrarCaso);
		this.add(botonGuardar);
		
	}
		
	private JButton crearBoton(String nombre) { 
		JButton boton = new JButton(nombre);
		boton.setAlignmentX(CENTER_ALIGNMENT);
		boton.addActionListener(controlador);
		return boton;
	}



	
	
	
}


