package fiuba.algo3.tp2.entities;

import java.io.Serializable;

import fiuba.algo3.tp2.interfaces.Pista;

public class PistaRasgo implements Pista  , Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String descripcion;
	private Rasgo rasgo;
	
	
	public String interactuar(Policia policia) {
		policia.pierdeTiempo(1); //entrar edificio
		policia.getOrden().agregarRasgo(rasgo);
		return descripcion;
	}


	public void setDescripcion(Pista pista) {
		this.descripcion = pista.getDescripcion();
	}


	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public void setearRasgo(Rasgo rasgo) {
		this.rasgo = rasgo;
		setDescripcion(rasgo.getDescripcion());
	}

}
