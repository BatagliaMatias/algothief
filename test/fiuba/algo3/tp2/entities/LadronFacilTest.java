package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.exceptions.ExcepcionCapitalesNoConectadas;
import fiuba.algo3.tp2.interfaces.Ladron;


public class LadronFacilTest {

	@Test
	public void testIdentificarObjetoParaRobar() {
		Ladron ladronFacil = new LadronFacil(null, null, null, null, null, null);
		String botin = "celular";
		assertEquals(ladronFacil.objetoRobado(), botin);
		
	}
	
	@Test
	public void testRutaDeEscape(){
		Ladron ladronFacil = new LadronFacil(null, null, null, null, null, null);
		int cantidadCapitales = 4;
		assertEquals(ladronFacil.rutaEscape(), cantidadCapitales);
	}
	
	@Test
	public void testViajesDisponibleFacil(){
		LadronGenerico ladron = new LadronFacil(null, null, null, null, null, null);
		assertTrue(ladron.viajeDisponible());
		
		Capital capital = new Capital("Buenos Aires","Argentina");
		Capital destino = new Capital("Roma","Italia");
	
		for(int i=0;i<4;i++){
		ladron.setUbicacion(capital);
		try {
			ladron.viajar(destino);
		} catch (ExcepcionCapitalesNoConectadas e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ladron.restarContador();
		}
			
		assertFalse(ladron.viajeDisponible()); 
		
	}
	
	
	
	
}
