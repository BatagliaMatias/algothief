package fiuba.algo3.tp2.factories;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.entities.LadronFacil;
import fiuba.algo3.tp2.interfaces.DificultadFactory;
import fiuba.algo3.tp2.interfaces.Ladron;


public class FacilFactoryTest {
/*
	@Test
	public void testFactoryCrearPistaFacil() {
		DificultadFactory factory = new FacilFactory();
		CreadorPista pistaFactory = factory.crearPista();
		assertEquals(pistaFactory.getClass(), CreadorPistaFacil.class);		
	}
	*/
	@Test
	public void testFactoryCrearLadronFacil(){
		DificultadFactory factory = new FacilFactory();
		Ladron ladronFactory = factory.crearLadron();
		assertEquals(ladronFactory.getClass(), LadronFacil.class);
	}

}
