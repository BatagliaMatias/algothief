package fiuba.algo3.tp2.entities;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;





public class ModificadorPoliciaXml implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void actualizar(String nombrePolicia, int cantidadCasos	) {
		Integer cantidadCasosInteger = new Integer(cantidadCasos);
		boolean modificado = false;
		String filepath = "xml/policias.xml";	
       	
		File archivo = new File(filepath);
		
		SAXBuilder constructorSAX = new SAXBuilder();
		  
		try {
		  Document  documento = (Document)constructorSAX.build(archivo);
		  /* Obtenemos el nodo raiz o principal */
		  Element nodoRaiz = documento.getRootElement();

		  /* Obtenemos la lista de los nodos con la etiqueta
		   * "aplicacion" que se encuentran en el nodo raiz */
		  List policias = nodoRaiz.getChildren("Policia");
		  
		  /* Recorremos esta lista imprimiendo los elementos
		   * dentro de cada aplicacion y su categoría */
		  int i = 0;
		  while(i<policias.size() && !modificado){
			  		

		      /* Obtenemos el elemento de la lista */
		      Element nodo = (Element)policias.get(i);

		      /* Obtenemos el atributo "categoria" del nodo */
		      String nombreXml = nodo.getAttribute("nombre").getValue();
		      
		      if(nombreXml.equalsIgnoreCase(nombrePolicia)){
		    	  nodo.setAttribute("casos", cantidadCasosInteger.toString());
		    	  modificado = true;
		      }
		   
		      i++; //aumenta antes asi el ultimo caso iguala al tamaño del archivo
		   
		      if(!modificado && i == policias.size()){
				  
				Element nodoAux = nodo.clone();
				nodoAux.setAttribute("casos", cantidadCasosInteger.toString());
				nodoAux.setAttribute("nombre", nombrePolicia);
				nodoRaiz.addContent(nodoAux);
				  
			  }
		  }
		  
		  
		//document is processed and edited successfully, lets save it in new file
	        XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
	        //output xml to console for debugging
	        //xmlOutputter.output(doc, System.out);
	        xmlOutputter.output(documento, new FileOutputStream(filepath));
		  
		  
		} catch (JDOMException e) {
		    System.out.println("Fichero XML no valido");
		    e.printStackTrace();
		} catch (IOException e) {
		    System.out.println("Fichero no valido");
		    e.printStackTrace();
		}

	}
}
	

