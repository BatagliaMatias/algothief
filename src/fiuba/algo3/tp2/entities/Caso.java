package fiuba.algo3.tp2.entities;

import java.io.Serializable;

import fiuba.algo3.tp2.entities.LadronGenerico.TipoRasgo;
import fiuba.algo3.tp2.exceptions.ExcepcionCapitalesNoConectadas;
import fiuba.algo3.tp2.interfaces.DificultadFactory;
import fiuba.algo3.tp2.interfaces.Ladron;
import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;
import fiuba.algo3.tp2.interfaces.Viajable;

public class Caso implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DificultadFactory dificultad;
	private Ladron ladron;
	private Capital capital_actual; //Capital donde estaria nuestra interfaz grafica == policia
	private Escenario escenario;
	private ObjetoRobado objeto;
	
	public Caso(DificultadFactory factory) {
		escenario = new Escenario();
		dificultad = factory;
		objeto = new ObjetoRobado();
		capital_actual = null;
		crearLadron();
	}
	
	////////////METODOS PARA GET//////////////
	
	public void crearLadron(){
		this.ladron =  dificultad.crearLadron();
	}
	
	public Ladron devolverLadron(){
		return this.ladron;
	}

	public String getLadronObjetoRobado() {
		return objeto.getNombreObjeto();
	}

	public String getRasgoLadron(TipoRasgo tiporasgo) {
		return ladron.getRasgo(tiporasgo).getDescripcion();
	}
	
	public String getNombreLadron() {
		return ladron.devolverNombreLadron();
	}
	
	public void setearCapital(Capital capital) {
		String nombreCapital = capital.getNombreCapital();
		this.capital_actual = escenario.obtenerCapital(nombreCapital);
	}
	
	public String getCapitalNombre() {
		return capital_actual.getNombreCapital();
	}
	
	public String getDescripcionLugar(TipoLugar lugar){
		return capital_actual.getLugar(lugar).getDescripcionLugar();
	}

	public String getCapitalPais() {
		return capital_actual.getNombrePais();
	}

	public String devolverDescripcionPista(TipoLugar lugar){
		return this.capital_actual.getLugar(lugar).devolverPista();
	}
	
	public Capital getCapitalActual(){
		return this.capital_actual;
	}
	
	public Escenario getEscenario(){
		return this.escenario;
	}

	public void ladronViaja(Capital destino) {
		try {
			((Viajable) ladron).viajar(destino);
			this.restarViajesPosibles();
		} catch (ExcepcionCapitalesNoConectadas e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void restarViajesPosibles(){
		ladron.restarContador();
	}
	

	public void setearUbicacionLadron(Capital capital) {
		
		((Viajable) ladron).setUbicacion(escenario.obtenerCapital(capital.getNombreCapital()));
	}
	
	public void setearUbicacionPoliciaEnCaso(Capital capital) {
		
		String nombreCapital = capital.getNombreCapital();
		this.capital_actual = escenario.obtenerCapital(nombreCapital);

	}
	
	public boolean ladronPuedeViajar(){
		return this.ladron.viajeDisponible();
	}

	public boolean capitalLadronEsInicial() {
		if ( ladron.esCapitalInicial() == true) return true;
		else return false;
	}

	public void ObjetoRobadoEnCapital(Capital origen) {
		objeto.leerObjetoDeCapital(origen);
	}

	public void setearUbicacionRoboLadron() {
		((Viajable) ladron).setUbicacion(escenario.obtenerCapital(objeto.getNombreCapital()));
	}

	public void setearUbicacionRoboPolicia() {
		
		this.capital_actual = escenario.obtenerCapital(objeto.getNombreCapital());
	}

	public Capital devolverCapitalRobo() {
		return escenario.obtenerCapital(objeto.getNombreCapital());
	}
}