package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;

public class PersistenciaTest {

	@Test
	public void testGuardarYCargar() {
		Persistencia persistencia = new Persistencia();
		Juego juego = new Juego ();
		
		LectorPoliciaXml lectorPolicia = new LectorPoliciaXml();
		String nombrePolicia = "PoliPersistencia";
		int casosPolicia = lectorPolicia.leerCasos(nombrePolicia);
		Policia policia = new Policia("PoliPersistencia",casosPolicia);
		
		Capital roma = new Capital("Roma","Italia");
		Capital paris = new Capital("Paris", "Francia");
		
		juego.agregarAgente(policia);	
		juego.crearCaso();
		juego.ladronRobaObjetoEn(paris);
		juego.policiaViajaA(paris);
		juego.ladronViajaA(roma);
		
		persistencia.guardarArchivo(juego);
		
		Juego juegoGuardado = persistencia.cargarAchivo("PoliPersistencia");
		
		assertEquals(juegoGuardado.devolverPista(TipoLugar.economico),"El sospechoso compro euros");
		
		
		
	}
	
	@Test
	public void testGuardarUnJuego() {
		Persistencia persistencia = new Persistencia();
		Juego juego = new Juego ();
		
		LectorPoliciaXml lectorPolicia = new LectorPoliciaXml();
		String nombrePolicia = "Franco";
		int casosPolicia = lectorPolicia.leerCasos(nombrePolicia);
		Policia policia = new Policia(nombrePolicia,casosPolicia);
		
		Capital roma = new Capital("Roma","Italia");
		Capital paris = new Capital("Paris", "Francia");
		
		juego.agregarAgente(policia);	
		juego.crearCaso();
		juego.ladronRobaObjetoEn(paris);
		juego.policiaViajaA(paris);
		juego.ladronViajaA(roma);
		
		persistencia.guardarArchivo(juego);
		
		assertTrue(persistencia.cargarAchivo(nombrePolicia) != null);
			
	}

	
	@Test
	public void testCargarUnJuego() {
		
		Persistencia persistencia = new Persistencia();
		Juego juego = new Juego ();
		
		String nombrePolicia = "Franco";
	
		juego = persistencia.cargarAchivo(nombrePolicia);
		
		assertEquals(juego.devolverPista(TipoLugar.economico),"El sospechoso compro una moneda europea");
		assertEquals(juego.devolverPista(TipoLugar.descriptivo),"El sospechoso viajo en un vehiculo con una bandera de colores verde, blanco y otro color");
		assertEquals(juego.devolverPista(TipoLugar.historico),"El sospechoso buscaba mapas del mediterraneo");
		
	}
}
