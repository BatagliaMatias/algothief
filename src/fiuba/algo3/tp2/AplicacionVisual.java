package fiuba.algo3.tp2;

import java.awt.BorderLayout;


import fiuba.algo3.tp2.controllers.Controlador;
import fiuba.algo3.tp2.entities.Juego;
import fiuba.algo3.tp2.entities.Persistencia;
import fiuba.algo3.tp2.entities.Policia;
import fiuba.algo3.tp2.views.VentanaPrincipal;

public class AplicacionVisual {
	
	private static Juego juego;
	private static VentanaPrincipal ventanaPrincipal;
	private static Controlador controladorPrincipal;
	
	public static void main(String[] args) {	
		
		iniciar();
	}

	

	public static void cargarJuego(String nombre) {
		
		Persistencia persistencia = new Persistencia();
		juego = persistencia.cargarAchivo(nombre);	
		actualizarNuevoJuego();
	}
	
	private static void actualizarNuevoJuego() {
		juego.deleteObservers();
		juego.addObserver(ventanaPrincipal);
		controladorPrincipal.setJuego(juego);
		ventanaPrincipal.setJuego(juego);
		
	}
	
	public static void iniciar(){
		
		juego = new Juego();
		Policia policia = new Policia("Random",0);	
		juego.agregarAgente(policia);
		ventanaPrincipal = new VentanaPrincipal(juego);
		controladorPrincipal = new Controlador(juego, policia, ventanaPrincipal);		
		juego.addObserver(ventanaPrincipal);	
		ventanaPrincipal.getPanelSeleccionPolicias().llenarDePolicias(controladorPrincipal);
		ventanaPrincipal.getPanelCargarPolicias().llenarDePolicias(controladorPrincipal);
		ventanaPrincipal.mostrar(ventanaPrincipal.getPanelSeleccionPolicias(),BorderLayout.WEST);
		ventanaPrincipal.mostrar(ventanaPrincipal.getPanelCargarPolicias(), BorderLayout.EAST);
		ventanaPrincipal.mostrar(ventanaPrincipal.getPanelCrearPolicia(), BorderLayout.CENTER);
		ventanaPrincipal.pack();
		ventanaPrincipal.setSize(800,600);
	
		
	}
}
