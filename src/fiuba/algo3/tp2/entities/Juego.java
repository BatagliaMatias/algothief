package fiuba.algo3.tp2.entities;



import java.io.Serializable;
import java.util.Observable;

import fiuba.algo3.tp2.exceptions.ExcepcionCapitalesNoConectadas;
import fiuba.algo3.tp2.factories.CasoFactory;
import fiuba.algo3.tp2.interfaces.Ladron;
import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;

public class Juego  extends Observable implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final int OK = 1;
	private static final int FALLO = 0;
	private Policia policia;
	private CasoFactory factoryCaso;
	private Caso caso;
	
	public Juego(){
		policia = null;
		factoryCaso = new CasoFactory();
	}
		
	public String saludar() {
		return "Bienvenido Policia al teclado, por favor identifiquese";
	}

	
	public boolean validarAgente(Policia policia) {
		return (this.policia.equals(policia));
	}

	public void agregarAgente(Policia policia) {
		this.policia = policia;
		this.actualizarObservadores();
	}

	public int crearCaso(){
		caso = factoryCaso.crearCasoRango(policia);
		return OK;
	}

	///////METODOS PARA LOS VIAJES DEL POLICIA Y LADRON/////////


	public void policiaViajaA(Capital destino) {

		try {
			caso.setearUbicacionPoliciaEnCaso(destino);
			policia.viajar(destino);	
			this.setChanged();
			this.notifyObservers();
		} catch (ExcepcionCapitalesNoConectadas e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	public int ladronViajaA(Capital destino) {
	

		if (ladronPuedeViajar() == true){
		caso.ladronViaja(destino);	
		caso.setearUbicacionLadron(destino);
		return OK;
		}
		else return FALLO;
		
	}
	
	///////METODOS PARA OBTENER/////////
	
	public Ladron devolverLadron() {
		return caso.devolverLadron();
	}
	
	public String devolverPista(TipoLugar lugar){
		return caso.devolverDescripcionPista(lugar);
	}
	
	public String interactuarPista(TipoLugar lugar){
		String salida = caso.getCapitalActual().getLugar(lugar).devolverPistaCompleta().interactuar(policia);
		actualizarObservadores();
		return salida;
	}

	public String devolverNombreCapitalActual(){
		return caso.getCapitalNombre();
	}
	
	public String devolverNombrePaisActual(){
		return caso.getCapitalPais();
	}

	public boolean ladronPuedeViajar() {
		 if (caso.ladronPuedeViajar() == true) return true;
		 else return false;
	}

	public int ladronRobaObjetoEn(Capital origen) {
		if (caso.capitalLadronEsInicial() == true){
			caso.ObjetoRobadoEnCapital(origen);
			caso.setearUbicacionLadron(origen);
			this.setChanged();
			this.notifyObservers();
			return OK;
		}
		else return FALLO;
	}
	
	public Caso getCaso(){
		return this.caso;
	}	
		
	public Policia getPolicia(){
		return policia;
	}

	public String devolverObjetoRobado() {
		return caso.getLadronObjetoRobado();
	}

	
	///METODOS AL AZAR PARA EL JUEGO
	
	public int ladronRobaObjetoEn() {
		if (caso.capitalLadronEsInicial() == true){
			caso.setearUbicacionRoboLadron();
			caso.setearUbicacionLadron(caso.devolverCapitalRobo());
			this.setChanged();
			this.notifyObservers();
			return OK;
			
		}
		else return FALLO;
	}
	

	public void policiaViajaA() {
		
		caso.setearUbicacionRoboPolicia();
		policia.viajarAInvestigarRobo(caso.devolverCapitalRobo());	
		this.setChanged();
		this.notifyObservers();
		
	}

	public void actualizarObservadores() {
		 setChanged();
	     notifyObservers();
	}

	public void iniciar() {
		this.crearCaso();
		this.ladronRobaObjetoEn();
		this.policiaViajaA();
	}
	

	public void ladronViaja(){
		if(ladronPuedeViajar()){ // el ladron viaja en el inicio del juego, y cada vez que el policia viaja, siempre y cuando pueda viajar
			String capitalDestino = this.getCaso().devolverLadron().determinarNombreProximaCiudad();
			Capital destino = this.getCaso().getEscenario().obtenerCapital(capitalDestino);
			this.ladronViajaA(destino);
		}
	}

}

