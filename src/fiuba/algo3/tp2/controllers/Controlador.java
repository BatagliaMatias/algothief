package fiuba.algo3.tp2.controllers;

import fiuba.algo3.tp2.entities.Juego;
import fiuba.algo3.tp2.entities.Policia;
import fiuba.algo3.tp2.views.VentanaPrincipal;

public class Controlador {
	
	//Controlador Principal//

	private VentanaPrincipal ventanaPrincipal;
	private ControladorSeleccionarPolicia controladorSeleccionarPolicia;
	private ControladorCargarPolicia controladorCargarPolicia;
	private ControladorAccionesPrincipales controladorAccionesPrincipales;
	private ControladorAccionesVisitarLugares controladorAccionesVisitarLugares;

	
	public Controlador(Juego juego, Policia policia, VentanaPrincipal vista){
		

		this.ventanaPrincipal = vista;
		this.controladorCargarPolicia = new ControladorCargarPolicia(juego,ventanaPrincipal,policia);
		this.controladorSeleccionarPolicia = new ControladorSeleccionarPolicia(juego,ventanaPrincipal,policia);
		this.controladorAccionesPrincipales = new ControladorAccionesPrincipales(juego,ventanaPrincipal,policia);
		this.controladorAccionesVisitarLugares = new ControladorAccionesVisitarLugares (juego, ventanaPrincipal, policia);
		ventanaPrincipal.setearControlador(this);
	}
	
	public ControladorSeleccionarPolicia getControlSeleccionarPolicia(){
		return controladorSeleccionarPolicia;
	}
	
	public ControladorCargarPolicia getControlCargarPolicia(){
		return controladorCargarPolicia;
	}
	
	public ControladorAccionesPrincipales getControladorAccionesPrincipales(){
		return controladorAccionesPrincipales;
	}

	public ControladorAccionesVisitarLugares getControladorAccionesLugares() {
		return controladorAccionesVisitarLugares;
	}


	public void setJuego(Juego juegoActual) {
		
		controladorSeleccionarPolicia.setJuego(juegoActual);
		controladorCargarPolicia.setJuego(juegoActual);
		controladorAccionesPrincipales.setJuego(juegoActual);
		controladorAccionesVisitarLugares.setJuego(juegoActual);
	
	}
	
}
