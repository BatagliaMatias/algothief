package fiuba.algo3.tp2.controllers;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import fiuba.algo3.tp2.entities.Capital;
import fiuba.algo3.tp2.entities.Juego;
import fiuba.algo3.tp2.views.VentanaPrincipal;

public class ControladorViajar implements ActionListener {

	
	private VentanaPrincipal vista;
	private Juego juego;

	public ControladorViajar(Juego juego, VentanaPrincipal ventanaPrincipal) {
		this.juego = juego;
		this.vista = ventanaPrincipal;
	}

	public void actionPerformed(ActionEvent e) {
		
		Map<String,Integer> conexiones = juego.getPolicia().getUbicacion().getConexiones();
			
		
		for (String key : conexiones.keySet()) {
			String capital = key + " a " + conexiones.get(key) +" KM";
			if( capital.equalsIgnoreCase(e.getActionCommand())){
				Capital destino = juego.getCaso().getEscenario().obtenerCapital(key);
				juego.policiaViajaA(destino);
				vista.repaint();
				if(juego.ladronPuedeViajar()){
					String capitalDestino = juego.getCaso().devolverLadron().determinarNombreProximaCiudad();
					Capital destinoLadron = juego.getCaso().getEscenario().obtenerCapital(capitalDestino);
					juego.ladronViajaA(destinoLadron);
				} else {
					juego.getCaso().devolverLadron().dejarTrampasYEsconderse(juego.getCaso().devolverLadron().getUbicacion());
				}
				
				vista.activarAccionesViajar();
			}
		}
		validarTiempo();
	}


	public void validarTiempo(){
		
		if(juego.getPolicia().estaCansado()){
			juego.getPolicia().dormir();
			vista.informarDormir();	
		}
		
		if(!juego.getPolicia().quedaTiempo()){
			vista.informarFinDelJuego(false);
			vista.iniciarNuevoJuego();
		}
	}

	public void setJuego(Juego juegoActual) {
		juego = juegoActual;	
	}
	
}
