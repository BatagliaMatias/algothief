package fiuba.algo3.tp2.viewsOld;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;

public class LayoutBienvenido extends LayoutBase{

	
	
	private static final long serialVersionUID = -9169380855988752584L;
	
	public static IdLayout id = new Id("LAYOUT_BIENVENIDO");
	JLabel tituloLabel;
	JButton nuevoUserButton;
	JButton tengoUserButton;
	
	public LayoutBienvenido(){
		setLayout(new GridLayout(0, 1));
		tituloLabel = new JLabel("Bienvenido", JLabel.CENTER);
		nuevoUserButton = new JButton("Nuevo Usuario");
		tengoUserButton = new JButton("Ya tengo usuario");
		add(tituloLabel);
		add(nuevoUserButton);
		add(tengoUserButton);
	}

	@Override
	public IdLayout getId() {
		return id;
	}
 
}
