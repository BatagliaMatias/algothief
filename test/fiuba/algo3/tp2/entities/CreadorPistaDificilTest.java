package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.interfaces.CreadorPista;
import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;

public class CreadorPistaDificilTest {

	@Test
	public void testPistaDificilEconomica(){
		CreadorPista pistaDificil = new CreadorLadronDificil();
		String ciudad = "Roma";
		String esperado = "El sospechoso compro billetes de 500";
		assertEquals(pistaDificil.darPista(ciudad,TipoLugar.economico), esperado);
				
	}
	
	@Test
	public void testPistadificilPuerto(){
		CreadorPista pistaDificil = new CreadorLadronDificil();
		String ciudad = "Roma";
		String esperado = "El sospechoso viajo en un vehiculo con una bandera de colores verde y dos colores mas";
		assertEquals(pistaDificil.darPista(ciudad,TipoLugar.descriptivo),esperado);
	}
	
	@Test
	public void testPistadificilBibloteca(){
		CreadorPista pistaDificil = new CreadorLadronDificil();
		String ciudad = "Roma";
		String esperado = "El sospechoso buscaba libros de latin";
		assertEquals(pistaDificil.darPista(ciudad,TipoLugar.historico), esperado);
	}
	
}
