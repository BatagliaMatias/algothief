package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;

import fiuba.algo3.tp2.entities.Capital;

public class CapitalTest {

	@Test
	public void testCrearCapitalConNombre() {
		String nombreCapital = "Roma";
		String nombrePais = "Italia";
		Capital capital = new Capital(nombreCapital, nombrePais);
		assertEquals(capital.getNombreCapital(), nombreCapital);
		assertEquals(capital.getNombrePais(), nombrePais);
	}
	
	@Test
	public void testConexiones(){
		String nombreCapital = "Roma";
		String nombrePais = "Italia";
		Capital capital = new Capital(nombreCapital, nombrePais);
		Map<String, Integer> conexiones = capital.getConexiones();
		assertTrue(conexiones.get("Paris").equals(1105));
		assertTrue(conexiones.get("Moscu").equals(2376));
		assertTrue(conexiones.get("Buenos Aires").equals(11125));
		assertTrue(!(conexiones.containsKey("Bagdad")));
		assertTrue(!(conexiones.containsKey("Roma")));
	}
	
	@Test
	public void testConexionDisponibleRomaParis(){
		String nombreCapital = "Roma";
		String nombrePais = "Italia";
		Capital capital = new Capital(nombreCapital, nombrePais);
		assertTrue(capital.conexionExiste("Paris"));
	}
	
	@Test
	public void testObtenerDistanciaRomaParis(){
		String nombreCapital = "Roma";
		String nombrePais = "Italia";
		Capital capital = new Capital(nombreCapital, nombrePais);
		assertTrue(capital.distanciaConexion("Paris").equals(1105));
	}
	
}
