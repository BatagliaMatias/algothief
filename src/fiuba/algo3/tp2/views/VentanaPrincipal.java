package fiuba.algo3.tp2.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import fiuba.algo3.tp2.AplicacionVisual;
import fiuba.algo3.tp2.controllers.Controlador;
import fiuba.algo3.tp2.entities.Juego;

public class VentanaPrincipal extends JFrame implements Observer {
	
	private static final long serialVersionUID = 1L;
	private Juego juego;
	private Container contenedor = getContentPane(); 
	String condicionBucle = "";
	private JPanel panelSuperior;
	private JPanel panelInferior;
	private JPanel panelIzquierdo;
	private JPanel panelDerecho;
	private JPanel panelCentral;
	private PanelSeleccionPolicia panelSeleccionPolicias;
	private PanelCargarPolicia panelCargarPolicia;
	private Map<String, JPanel> ubicaciones = new HashMap<String,JPanel>();
	private PanelAccionesPrincipales panelAccionesPrincipales;
	private PanelVisitarLugares panelVisitarLugares;
	private Controlador controlador;
	private PanelTiempoUbicacion panelTiempoUbicacion;
	private PanelOrdenDeArresto panelOrdenDeArresto;
	private PanelCapitalesConectadas panelCapitalesConectadas;
	private PanelCrearPolicia panelCrearPolicia;
	
	public VentanaPrincipal(Juego juego){
					
		this.juego = juego;
		setSize(1024,768);	  
		this.setExtendedState(this.MAXIMIZED_BOTH);  
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	    setVisible(true);	
		setTitle("AlgoThief"); 
		panelSuperior = new JPanel();
	    panelDerecho = new JPanel();
	    panelCentral = new JPanel();
	    panelIzquierdo = new JPanel();
	    panelInferior = new JPanel();
	    panelSeleccionPolicias = new PanelSeleccionPolicia();	
	    panelCargarPolicia = new PanelCargarPolicia();
	    panelAccionesPrincipales = null;
	    panelVisitarLugares = null;
	    panelCrearPolicia = new PanelCrearPolicia(this);  
	    contenedor.setLayout (new BorderLayout()); // separa la pantalla en 5 partes
	    panelInferior.setBorder(BorderFactory.createMatteBorder(4,4,4,4,Color.BLACK));
	    panelInferior.setBackground(Color.black);
	    JLabel superior = new JLabel("ALGOTHIEF - FIUBA ALGO3");
	    JLabel central = new JLabel("Soy la parte central, tengo lo q sobra de las otras 4");
	    JLabel derecha = new JLabel("Soy la parte derecha");
	    JLabel abajo = new JLabel("Bataglia - Mendiola - Gonzalez");
	   
	    panelSuperior.add(superior);
	    panelSuperior.setVisible(true);    
	    panelDerecho.add(derecha);
	    panelDerecho.setVisible(true);	    
	    panelCentral.add(central);  
	    panelInferior.add(abajo);
	    panelInferior.setVisible(true);
	        
	    contenedor.add (panelCentral, BorderLayout.CENTER);
	    contenedor.add (panelSuperior, BorderLayout.NORTH);
	    contenedor.add (panelInferior, BorderLayout.SOUTH);
	    contenedor.add (panelIzquierdo, BorderLayout.WEST);
	    contenedor.add (panelDerecho, BorderLayout.EAST);
	    
	    ubicaciones.put(BorderLayout.CENTER,panelCentral);
	    ubicaciones.put(BorderLayout.NORTH,panelSuperior);
	    ubicaciones.put(BorderLayout.SOUTH, panelInferior);
	    ubicaciones.put(BorderLayout.WEST, panelIzquierdo);
	    ubicaciones.put(BorderLayout.EAST, panelDerecho);
	    
	    this.pack();
    }


	public void update(Observable o, Object arg) {
		
		
	}
	
	public void agregarTexto(String string){
		 
		 JLabel label = new JLabel(string);
		 JPanel panel = new JPanel();
		 panel.add(label);
		 panel.setVisible(true);
		 contenedor.add(panel,BorderLayout.CENTER);
		 
	}

	public JPanel getPanelSuperior() {
		return panelSuperior;
	}

	public JPanel getPanelInferior() {
		return panelInferior;
	}

	public JPanel getPanelIzquierdo() {
		return panelIzquierdo;
	}

	public JPanel getPanelDerecho() {
		return panelDerecho;
	}

	public JPanel getPanelCentral() {
		return panelCentral;
	}

	public PanelSeleccionPolicia getPanelSeleccionPolicias() {
		return panelSeleccionPolicias;
	}
	
	public PanelCrearPolicia getPanelCrearPolicia(){
		return panelCrearPolicia;
	}
	
	public void mostrar(JPanel panel,String ubicacion) {
		
		panel.setAlignmentX(CENTER_ALIGNMENT);
		panel.setAlignmentY(CENTER_ALIGNMENT);
		contenedor.remove(ubicaciones.get(ubicacion));
		ubicaciones.remove(ubicacion);
		ubicaciones.put(ubicacion, panel);
		contenedor.add(panel,ubicacion);	
		this.validate();
		this.repaint();
		
	}

	public void iniciarJuego() {
		
		juego.getPolicia().inicializarRango();
		juego.crearCaso();
		juego.ladronRobaObjetoEn();
		juego.policiaViajaA();
		juego.ladronViaja();	
		JPanel panel = new JPanel();
		setVisible(true);	
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));	
		setBackground(Color.LIGHT_GRAY);
		String ruta = "images/"+juego.devolverNombreCapitalActual()+".jpg";
		Imagen imagen = new Imagen(ruta);
		imagen.setVisible(true);
		mostrar(imagen,BorderLayout.CENTER);
		mostrar(panelAccionesPrincipales,BorderLayout.WEST);
		panelTiempoUbicacion = new PanelTiempoUbicacion(juego);
		mostrar(panelTiempoUbicacion,BorderLayout.NORTH);
		this.activarPanelOrdenDeArresto();
		
	}
	public void cargarJuego(){
		
		JPanel panel = new JPanel();
		setVisible(true);	
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));	
		setBackground(Color.LIGHT_GRAY);	
		String ruta = "images/"+juego.devolverNombreCapitalActual()+".jpg";
		Imagen imagen = new Imagen(ruta);
		imagen.setVisible(true);
		mostrar(imagen,BorderLayout.CENTER);
		mostrar(panelAccionesPrincipales,BorderLayout.WEST);
		panelTiempoUbicacion = new PanelTiempoUbicacion(juego);
		mostrar(panelTiempoUbicacion,BorderLayout.NORTH);
		this.activarPanelOrdenDeArresto();
	}
	
	
	public void activarAccionesViajar() {
		
		
		this.panelCentral.repaint();
		panelInferior.setBorder(BorderFactory.createMatteBorder(4,4,4,4,Color.BLACK));
		panelInferior.setBackground(Color.black);	
		mostrar(panelInferior,BorderLayout.SOUTH);
		JPanel panel = new JPanel();
		String ruta = "images/"+juego.devolverNombreCapitalActual()+".jpg";
		Imagen imagen = new Imagen(ruta);
		imagen.setVisible(true);
		setVisible(true);
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		setBackground(Color.LIGHT_GRAY);
		mostrar(imagen,BorderLayout.CENTER);	
	}

	public void activarCapitalesConectadas() {
		this.panelIzquierdo.repaint();
		panelCapitalesConectadas = new PanelCapitalesConectadas(juego);
		mostrar(panelCapitalesConectadas, BorderLayout.CENTER);
		
	}

	public void activarAccionesVisitarLugar() {
		
		panelVisitarLugares= new PanelVisitarLugares(controlador.getControladorAccionesLugares());
		mostrar(panelVisitarLugares,BorderLayout.WEST);
	}
	
	public void activarPanelOrdenDeArresto(){
		
		panelOrdenDeArresto = new PanelOrdenDeArresto(juego);
		panelOrdenDeArresto.setVisible(true);
		mostrar(panelOrdenDeArresto, BorderLayout.EAST);
	}

	public void setearControlador(Controlador controlador) {
		this.controlador = controlador;
	    panelAccionesPrincipales = new PanelAccionesPrincipales(controlador.getControladorAccionesPrincipales());	
	}

	public PanelAccionesPrincipales getPanelAccionesPrincipales() {
		return panelAccionesPrincipales;
	}

	public void activarPanelAccionesPrincipales() {
		
		this.panelIzquierdo.repaint();
		mostrar(panelAccionesPrincipales,BorderLayout.WEST);

	}

	public void MostrarPistaEnPanelCentral(String devolverPista) {

		this.panelCentral.repaint();
		Imagen imagen = new Imagen("images/lupa.jpg");
		JPanel panel = new JPanel();
		JLabel encontrasteAlgo = new JLabel("                                    Has encontrado una pista                              ");
		JLabel label = new JLabel(devolverPista);
		JLabel vacio = new JLabel("          ");
		setVisible(true);
		String pista = devolverPista;
		String pistaParte = "";
		int letras = pista.length();
		int letrasParcial = 0;		
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));	
		setBackground(Color.LIGHT_GRAY);
		panel.setBackground(Color.LIGHT_GRAY);
		encontrasteAlgo.setFont(new Font("TAHOMA", Font.ITALIC, 30));
		label.setFont(new Font("TAHOMA", Font.ITALIC, 30));			
	    label.setBackground(Color.black);
		JPanel pistaPanel = new JPanel();
		if(!pista.equalsIgnoreCase("No he visto a nadie sospechoso")){
			panel.add(imagen);
			pistaPanel.add(encontrasteAlgo);
		}	
		while(letras>60){
			pistaParte = pista.substring(0, 60);
			pistaParte = pistaParte.substring(0,pistaParte.lastIndexOf(" "));
			letrasParcial = pistaParte.length();
			pista = pista.substring(letrasParcial,pista.length());
			letras = letras - letrasParcial;
			pistaPanel.add(crearLabelPista(pistaParte));
		}	
		pistaPanel.add(crearLabelPista(pista));	
		pistaPanel.setBorder(BorderFactory.createMatteBorder(2,2,2,2,Color.DARK_GRAY));
		panel.add(pistaPanel);	
		panel.setVisible(true);
		mostrar(panel,BorderLayout.CENTER);		
		panel.add(vacio);

	}
	
	
	public void MostrarPistaRasgoEnPanelCentral(String devolverPista) {

		this.panelCentral.repaint();
		Imagen imagen = new Imagen("images/lupa.jpg");
		JPanel panel = new JPanel();
		JLabel encontrasteAlgo = new JLabel("                                    Has encontrado una pista                              ");
		JLabel label = new JLabel(devolverPista);
		JLabel vacio = new JLabel("          ");
		setVisible(true);
		String pista = devolverPista;

		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));	
		setBackground(Color.LIGHT_GRAY);
		panel.setBackground(Color.LIGHT_GRAY);
		encontrasteAlgo.setFont(new Font("TAHOMA", Font.ITALIC, 30));
		label.setFont(new Font("TAHOMA", Font.ITALIC, 30));			
	    label.setBackground(Color.black);		    		
		JPanel pistaPanel = new JPanel();
		
		if(!pista.equalsIgnoreCase("No he visto a nadie sospechoso")){
			panel.add(imagen);
			pistaPanel.add(encontrasteAlgo);
			pistaPanel.setBorder(BorderFactory.createMatteBorder(2,2,2,2,Color.DARK_GRAY));
			pistaPanel.setVisible(true);			
			Imagen rasgo = new Imagen("rasgos/"+pista+".png");
			panel.add(rasgo);
		}
		else{
			JLabel nada = new JLabel(pista);
			nada.setFont(new Font("TAHOMA", Font.ITALIC, 30));
			pistaPanel.add(nada);
			pistaPanel.setBorder(BorderFactory.createMatteBorder(2,2,2,2,Color.DARK_GRAY));
			pistaPanel.setVisible(true);
			panel.add(pistaPanel);
		}	
		panel.setVisible(true);
		mostrar(panel,BorderLayout.CENTER);	
		panel.add(vacio);
}
	
	
	public void MostrarCuchilloEnPanelCentral() {
		this.panelCentral.repaint();
		Imagen imagen = new Imagen("images/cuchillo.jpg");
		JPanel panel = new JPanel();
		JLabel encontrasteAlgo = new JLabel("                                      Te han acuchillado!                              ");
		JLabel vacio = new JLabel("          ");
		setVisible(true);
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));	
		setBackground(Color.LIGHT_GRAY);
		panel.setBackground(Color.LIGHT_GRAY);
		encontrasteAlgo.setFont(new Font("TAHOMA", Font.ITALIC, 30));
		JPanel pistaPanel = new JPanel();
		pistaPanel.setBorder(BorderFactory.createMatteBorder(2,2,2,2,Color.DARK_GRAY));
		pistaPanel.setVisible(true);
		panel.add(encontrasteAlgo);
		panel.add(imagen);
		panel.setVisible(true);
		mostrar(panel,BorderLayout.CENTER);
		panel.add(vacio);
	}
	
	
	public void MostrarDisparoEnPanelCentral() {
		this.panelCentral.repaint();
		Imagen imagen = new Imagen("images/revolver.jpg");
		JPanel panel = new JPanel();
		JLabel encontrasteAlgo = new JLabel("                                      Te han disparado!                              ");
		JLabel vacio = new JLabel("          ");
		setVisible(true);
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));	
		setBackground(Color.LIGHT_GRAY);
		panel.setBackground(Color.LIGHT_GRAY);
		encontrasteAlgo.setFont(new Font("TAHOMA", Font.ITALIC, 30));
		JPanel pistaPanel = new JPanel();
		pistaPanel.setBorder(BorderFactory.createMatteBorder(2,2,2,2,Color.DARK_GRAY));
		pistaPanel.setVisible(true);
		panel.add(encontrasteAlgo);
		panel.add(imagen);
		panel.setVisible(true);
		mostrar(panel,BorderLayout.CENTER);
		panel.add(vacio);
	}
	
	private JLabel crearLabelPista(String texto){
		JLabel label = new JLabel(texto);
		label.setFont(new Font("TAHOMA", Font.ITALIC, 30));
		return label;
	}
	
	public void informarDormir(){
		JOptionPane.showMessageDialog(this, "Ha llegado la noche y "+juego.getPolicia().getNombre()+" se ha ido a dormir");
	}
	
	public void informarFinDelJuego(boolean gano){
		
		if(gano){
			JOptionPane.showMessageDialog(this, "Felicitaciones, Ganaste!");
					
		}
		else{
			JOptionPane.showMessageDialog(this, "Has perdido el rasto del ladron, intentalo de nuevo");
			
		}
	}

	public PanelCargarPolicia getPanelCargarPolicias() {
		return panelCargarPolicia;
	}

	public void setJuego(Juego juegoActual) {
		juego = juegoActual;
		
	}
	
	
	public void policiaCreado(String nombre){
		JOptionPane.showMessageDialog(this, "Bienvenido a la Academia Novato "+nombre+"!");
		panelSeleccionPolicias.llenarDePolicias(controlador);
		panelSeleccionPolicias.validate();
		panelSeleccionPolicias.repaint();
	}

	public void informarGuardado() {
		JOptionPane.showMessageDialog(this, "Juego Guardado");
		
	}

	public void informarAcuchillado() {
		JOptionPane.showMessageDialog(this,"Has sido acuchillado!");
		
	}

	public void informarDisparo() {
		JOptionPane.showMessageDialog(this,"Te han disparado!");
		
	}

	public void informarLadronAtrapado() {
		JOptionPane.showMessageDialog(this,"Has arrestado al ladron, Felicitaciones! ");
		
			if (juego.getPolicia().getCantidad_arrestos()==5){
				JOptionPane.showMessageDialog(this,"Has sido ascendido a Detective");
				iniciarNuevoJuego();
			}
			else if (juego.getPolicia().getCantidad_arrestos()==10){
				JOptionPane.showMessageDialog(this,"Has sido ascendido a Investigador");
				iniciarNuevoJuego();
			}
			else if (juego.getPolicia().getCantidad_arrestos()==20){
				JOptionPane.showMessageDialog(this,"Has sido ascendido a Sargento");
				iniciarNuevoJuego();
			}
		
			else{
				iniciarJuego();
			}
		
	}

	public void iniciarNuevoJuego() {
		JOptionPane.showMessageDialog(this, "Caso Cerrado");	
		dispose();
		
		AplicacionVisual.iniciar();
	}


}
