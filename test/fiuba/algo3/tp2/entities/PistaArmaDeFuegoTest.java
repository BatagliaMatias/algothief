package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.interfaces.Pista;

public class PistaArmaDeFuegoTest {

	@Test
	public void testPistaArmaDeFuego() {
		Pista pistaArmaDeFuego = new PistaArmaDeFuego();
		assertEquals(pistaArmaDeFuego.getDescripcion(),"Te han disparado y has sido herido");
	}
	
	@Test
	public void testPistaArmaDeFuegoPolicia() {
		Policia policia = new Policia("Mat",0);
		Pista pistaArmaDeFuego = new PistaArmaDeFuego();
		assertEquals(pistaArmaDeFuego.interactuar(policia),"Te han disparado y has sido herido");
		assertEquals(policia.getHora(),"Lunes 12:00");
	}

}
