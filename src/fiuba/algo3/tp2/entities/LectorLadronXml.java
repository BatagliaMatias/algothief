package fiuba.algo3.tp2.entities;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LectorLadronXml implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HashMap<String, String> obtenerLadron(int numeroLadron){ //del 1 al Limite
		
		
		Document document;
		
		try {
			HashMap<String, String> ladron = new HashMap<String, String>();
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.parse(new File("xml/ladrones.xml"));
			NodeList nodeList = document.getDocumentElement().getChildNodes();
			int contadorNodoLadron = 0;
			for (int i = 0; i < nodeList.getLength(); i++) {
               Node node = nodeList.item(i);
               if (node.getNodeType() == Node.ELEMENT_NODE) {
            	  
            	  contadorNodoLadron++;
            	  if(contadorNodoLadron == numeroLadron){
            		  
            	  
                  ladron.put("nombre", node.getAttributes().getNamedItem("nombre").getNodeValue());                    
                  ladron.put("sexo", node.getAttributes().getNamedItem("sexo").getNodeValue());
                  ladron.put("hobby", node.getAttributes().getNamedItem("hobby").getNodeValue()); 
                  ladron.put("cabello", node.getAttributes().getNamedItem("cabello").getNodeValue()); 
                  ladron.put("senia", node.getAttributes().getNamedItem("senia").getNodeValue()); 
                  ladron.put("vehiculo", node.getAttributes().getNamedItem("vehiculo").getNodeValue());
                  return ladron;
            	  }                 
               
			}
            
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
		
		
	}
	

}
