package fiuba.algo3.tp2.entities;

import java.io.Serializable;

import fiuba.algo3.tp2.interfaces.CreadorPista;
import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;

public class CreadorLadronFacil implements CreadorPista , Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String descripcion;
	private LectorPistaXml lector;
	private static String dificultad = "facil";

	public CreadorLadronFacil(String descripcionPista) {
		descripcion = descripcionPista;
	}

	public CreadorLadronFacil() {
		lector = new LectorPistaXml();
	}

	public String descripcion() {
		return descripcion;
	}

	public String darPista(String ciudad,TipoLugar lugar){
		return lector.leerPista(ciudad, dificultad, lugar.toString()).getDescripcion();
	}

}
