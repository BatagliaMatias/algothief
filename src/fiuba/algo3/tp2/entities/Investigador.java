package fiuba.algo3.tp2.entities;

import java.io.Serializable;

import fiuba.algo3.tp2.interfaces.Rangos;

public final class Investigador implements Rangos , Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final Integer velocidadDesplazamiento = 1300;
	final String nombreRango = "Investigador";
	
	public Integer getVelocidadDesplazamiento() {
		return velocidadDesplazamiento;
	}

	public String getNombreRango() {
		return nombreRango;
	}

}
