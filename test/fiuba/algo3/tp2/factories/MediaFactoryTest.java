package fiuba.algo3.tp2.factories;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.entities.LadronMedia;
import fiuba.algo3.tp2.interfaces.DificultadFactory;
import fiuba.algo3.tp2.interfaces.Ladron;

public class MediaFactoryTest {

	/*
	@Test
	public void testFactoryCrearPistaMedia() {
		DificultadFactory factory = new MediaFactory();
		CreadorPista pistaFactory = factory.crearPista();
		assertEquals(pistaFactory.getClass(), CreadorPistaMedia.class);		
	}
*/
	@Test
	public void testFactoryCrearLadronMedia(){
		DificultadFactory factory = new MediaFactory();
		Ladron ladronFactory = factory.crearLadron();
		assertEquals(ladronFactory.getClass(), LadronMedia.class);
	}
}
