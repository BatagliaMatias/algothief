package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.entities.CreadorLadronFacil;
import fiuba.algo3.tp2.interfaces.CreadorPista;
import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;

public class CreadorPistaFacilTest {

	@Test
	public void testCrearPistaConDescripcion() {
		String descripcionPista = "Se ha ido en un vehiculo con una bandera color roja";
		CreadorLadronFacil pistaFacil = new CreadorLadronFacil(descripcionPista);
		assertEquals(pistaFacil.descripcion(), descripcionPista);
	}
	
	@Test
	public void testPistaFacilEconomica(){
		CreadorPista pistaFacil = new CreadorLadronFacil();
		String ciudad = "Roma";
		String esperado = "El sospechoso compro euros";
		assertEquals(pistaFacil.darPista(ciudad,TipoLugar.economico), esperado);
				
	}
	
	@Test
	public void testPistaFacilPuerto(){
		CreadorPista pistaFacil = new CreadorLadronFacil();
		String ciudad = "Roma";
		String esperado = "El sospechoso viajo en un vehiculo con una bandera de colores verde, blanco y rojo";
		assertEquals(pistaFacil.darPista(ciudad,TipoLugar.descriptivo),esperado);
	}
	
	@Test
	public void testPistaFacilBibloteca(){
		CreadorPista pistaFacil = new CreadorLadronFacil();
		String ciudad = "Roma";
		String esperado = "El sospechoso buscaba libros de gladiadores";
		assertEquals(pistaFacil.darPista(ciudad,TipoLugar.historico), esperado);
	}
	
	
	
	
}
