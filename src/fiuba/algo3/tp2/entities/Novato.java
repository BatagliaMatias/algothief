package fiuba.algo3.tp2.entities;

import java.io.Serializable;

import fiuba.algo3.tp2.interfaces.Rangos;

public final class Novato implements Rangos , Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final Integer VelocidadDesplazamiento = 900;
	final String nombreRango = "Novato";
	
	public Integer getVelocidadDesplazamiento() {
		return VelocidadDesplazamiento;
	}
	
	public String getNombreRango() {
		
		return nombreRango;
	}
}

