package fiuba.algo3.tp2.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fiuba.algo3.tp2.AplicacionVisual;
import fiuba.algo3.tp2.entities.Juego;
import fiuba.algo3.tp2.entities.Policia;
import fiuba.algo3.tp2.views.VentanaPrincipal;

public class ControladorCargarPolicia implements ActionListener{

	private Juego juego;
	private VentanaPrincipal vista;
	private Policia policia;
	private AplicacionVisual main;


	public ControladorCargarPolicia(Juego juego,VentanaPrincipal ventanaPrincipal, Policia policia) {
		this.juego = juego;
		this.vista = ventanaPrincipal;
		this.policia = policia;
	
	}

	
	public void actionPerformed(ActionEvent e) {
		
		String nombre = e.getActionCommand();
		nombre = nombre.substring(0,nombre.indexOf("."));
		AplicacionVisual.cargarJuego(nombre);    
	    vista.cargarJuego();
	}


	public void setJuego(Juego juegoActual) {
		juego = juegoActual;
		policia = juegoActual.getPolicia();
		
	} 
}
