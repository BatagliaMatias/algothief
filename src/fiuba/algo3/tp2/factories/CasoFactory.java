package fiuba.algo3.tp2.factories;

import java.io.Serializable;

import fiuba.algo3.tp2.entities.Caso;
import fiuba.algo3.tp2.entities.Policia;
import fiuba.algo3.tp2.interfaces.DificultadFactory;

public class CasoFactory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DificultadFactory factory;

	public Caso crearCasoRango(Policia policia){
		if (policia.rango() == "Novato"){
			factory = new FacilFactory();
			return new Caso(factory);	
		}
		if (policia.rango() == "Detective"){
			factory = new MediaFactory();
			return new Caso(factory);	
		}
		if (policia.rango() == "Investigador"){
			factory = new MediaFactory();
			return new Caso(factory);	
		}
		if (policia.rango() == "Sargento"){
			factory = new DificilFactory();
			return new Caso(factory);	
		}
		return null;
	}
}