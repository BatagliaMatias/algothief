package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.interfaces.Pista;

public class PistaInteresTest {

	@Test
	public void testPistaFacilPuerto(){
		Pista pistaInteres = new PistaInteres();
		LectorPistaXml lector = new LectorPistaXml();
		String dificultad = "dificil";
		String tipo = "descriptivo";
		String ciudad = "Roma";
		String esperado = "El sospechoso viajo en un vehiculo con una bandera de colores verde y dos colores mas";
		pistaInteres.setDescripcion(lector.leerPista(ciudad, dificultad, tipo));
		assertEquals(pistaInteres.getDescripcion(), esperado);
				
	}
	
	
	
	
}
