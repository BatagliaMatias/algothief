package fiuba.algo3.tp2.views;


import java.awt.Color;
import java.awt.Font;
import java.io.File;


import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fiuba.algo3.tp2.controllers.Controlador;
import fiuba.algo3.tp2.controllers.ControladorCargarPolicia;

public class PanelCargarPolicia extends JPanel {
		private static final long serialVersionUID = 1L;
		
		public PanelCargarPolicia(){
			setVisible(true);
			setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
			setBackground(Color.LIGHT_GRAY);
			JLabel label = new JLabel("Cargar Partida");
			Font labelFont = label.getFont();
			label.setFont(new Font(labelFont.getName(), Font.PLAIN, 30));
			label.setAlignmentX(CENTER_ALIGNMENT);
			this.add(label);
			
		}
		
		public void llenarDePolicias(Controlador control){
			
			File dir = new File("saved/");
			String[] ficheros = dir.list();
			
			if (ficheros == null)
				  System.out.println("No hay ficheros en el directorio especificado");
				else { 
				  for (int x=0;x<ficheros.length;x++)
					  this.add(crearBoton(ficheros[x],control.getControlCargarPolicia()));
				}
			
			
			//ArrayList<String> policias = new ArrayList<String>();
			
			//policias.add("Mat");
			//policias.add("otro");
			
			
		}
		
		private JButton crearBoton(String nombre, ControladorCargarPolicia controladorCargarPolicia) {
			
			JButton boton = new JButton(nombre);
			boton.setAlignmentX(CENTER_ALIGNMENT);
			boton.addActionListener(controladorCargarPolicia);
			return boton;
		}
		
		
	

}
