package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Test;



public class LectorLadronXmlTest {

	@Test
	public void testLeerLadron() {
		LectorLadronXml lector = new LectorLadronXml();
		int numeroLadron = 1; //pido crear el ladron numero 1
		HashMap<String, String> ladronDatos = lector.obtenerLadron(numeroLadron);
		assertTrue(ladronDatos.get("nombre").equals("Nick Brunch"));
		assertTrue(ladronDatos.get("sexo").equals("Masculino"));
		assertTrue(ladronDatos.get("cabello").equals("Negro"));
		assertTrue(ladronDatos.get("senia").equals("Anillo"));
		assertTrue(ladronDatos.get("vehiculo").equals("Moto"));
		assertTrue(ladronDatos.get("hobby").equals("Alpinismo"));
		
	}
	
	@Test
	public void testLeerLadron2(){
		LectorLadronXml lector = new LectorLadronXml();
		int numeroLadron = 2; //pido crear el ladron numero 2
		HashMap<String, String> ladronDatos = lector.obtenerLadron(numeroLadron);
		assertTrue(ladronDatos.get("nombre").equals("Len Bulk"));
		
	}

	@Test
	public void testLeerOtroLadron() {
		LectorLadronXml lector = new LectorLadronXml();
		int numeroLadron = 3; 
		HashMap<String, String> ladronDatos = lector.obtenerLadron(numeroLadron);
		assertTrue(ladronDatos.get("nombre").equals("Ihor Ihorovitch"));
		assertTrue(ladronDatos.get("sexo").equals("Masculino"));
		assertTrue(ladronDatos.get("cabello").equals("Rubio"));
		assertTrue(ladronDatos.get("senia").equals("Tatuaje"));
		assertTrue(ladronDatos.get("vehiculo").equals("Limusina"));
		assertTrue(ladronDatos.get("hobby").equals("Croquet"));
		
	}
	
	@Test
	public void testLeerOtroLadron2() {
		LectorLadronXml lector = new LectorLadronXml();
		int numeroLadron = 6; 
		HashMap<String, String> ladronDatos = lector.obtenerLadron(numeroLadron);
		assertTrue(ladronDatos.get("nombre").equals("Merey Laroc"));
		assertTrue(ladronDatos.get("sexo").equals("Femenino"));
		assertTrue(ladronDatos.get("cabello").equals("Marron"));
		assertTrue(ladronDatos.get("senia").equals("Joyas"));
		assertTrue(ladronDatos.get("vehiculo").equals("Limusina"));
		assertTrue(ladronDatos.get("hobby").equals("Alpinismo"));
		
	}
	
	
	@Test
	public void testLeerCarmen(){
		LectorLadronXml lector = new LectorLadronXml();
		int numeroLadron = 10;
		HashMap<String, String> ladronDatos = lector.obtenerLadron(numeroLadron);
		assertTrue(ladronDatos.get("nombre").equals("Carmen Sandiego"));
	}
	
}
