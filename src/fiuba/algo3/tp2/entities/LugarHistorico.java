package fiuba.algo3.tp2.entities;

import java.io.Serializable;

import fiuba.algo3.tp2.interfaces.Lugar;
import fiuba.algo3.tp2.interfaces.Pista;

public class LugarHistorico implements Lugar,Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String descripcionLugar;
	private Pista pistaHistorica;
	
	public LugarHistorico(){
		descripcionLugar= "Museo";
	}

	public String getDescripcionLugar() {
		return descripcionLugar;
	}


	public void setDescripcionLugar(String descripcionLugar) {
		this.descripcionLugar = descripcionLugar;
	}
	
	public void setearPista(Pista pista) {
		this.pistaHistorica = pista;
	}

	public String devolverPista() {
		return pistaHistorica.getDescripcion();
	}

	public TipoLugar getTipoLugar() {
		return TipoLugar.historico;
	}
	
	public Pista devolverPistaCompleta() {
		return pistaHistorica;
	}
}
