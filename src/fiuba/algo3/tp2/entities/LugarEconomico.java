package fiuba.algo3.tp2.entities;

import java.io.Serializable;

import fiuba.algo3.tp2.interfaces.Lugar;
import fiuba.algo3.tp2.interfaces.Pista;

public class LugarEconomico implements Lugar, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String descripcionLugar;
	private Pista pistaEconomica;
	
	public LugarEconomico() {
		this.descripcionLugar = "Bolsa de Valores";
	}


	public String devolverPista() {
		return pistaEconomica.getDescripcion();
	}


	public String getDescripcionLugar() {
		return descripcionLugar;
	}


	public void setDescripcionLugar(String descripcionLugar) {
		this.descripcionLugar = descripcionLugar;
	}

	public void setearPista(Pista pistaEconomica) {
		this.pistaEconomica = pistaEconomica;
	}

	public TipoLugar getTipoLugar() {
		return TipoLugar.economico;
	}
	
	public Pista devolverPistaCompleta() {
		return pistaEconomica;
	}

}
