package fiuba.algo3.tp2.controllers;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fiuba.algo3.tp2.entities.Juego;
import fiuba.algo3.tp2.entities.Policia;
import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;
import fiuba.algo3.tp2.views.VentanaPrincipal;

public class ControladorAccionesVisitarLugares implements ActionListener {
	
	private Juego juego;
	private VentanaPrincipal vista;
	private Policia policia;
	private enum tipos{cuchillo,fuego,comun,ladron};
	private String pista;
	
	public ControladorAccionesVisitarLugares(Juego juego, VentanaPrincipal ventanaPrincipal, Policia policia) {
		
		this.juego = juego;
		this.vista = ventanaPrincipal;
		this.policia = policia; 
	}

	public void actionPerformed(ActionEvent e) {
		
		
		if("Recorrer la Calle".equalsIgnoreCase(e.getActionCommand())){
			pista = juego.interactuarPista(TipoLugar.rasgo);
			validarPistaRasgo(pista);
			validarTiempo();
		}
		
		else if("Visitar Museo".equalsIgnoreCase(e.getActionCommand())){
			pista = juego.interactuarPista(TipoLugar.historico);
			validarPistaComun(pista);
			validarTiempo();
		}
		
		else if("Investigar Banco".equalsIgnoreCase(e.getActionCommand())){
			pista = juego.interactuarPista(TipoLugar.economico);
			validarPistaComun(pista);
			validarTiempo();
		}
		
		else if("Investigar el Aeropuerto".equalsIgnoreCase(e.getActionCommand())){
			pista = juego.interactuarPista(TipoLugar.descriptivo);
			validarPistaComun(pista);
			validarTiempo();
		}
		
		else if("Volver al Menu Anterior".equalsIgnoreCase(e.getActionCommand())){
			vista.activarPanelAccionesPrincipales();
		}
	}
	
	public void validarTiempo(){
		
		if(juego.getPolicia().estaCansado()){
			juego.getPolicia().dormir();
			vista.informarDormir();		
		}
		if(!(juego.getPolicia().quedaTiempo())){
			vista.informarFinDelJuego(false);
			vista.iniciarNuevoJuego();
		}
		
	}

	public void setJuego(Juego juegoActual) {
		juego = juegoActual;
		policia = juegoActual.getPolicia();
		
	}
	
	public void validarPistaComun(String pista){
		if(validarPista(pista).equals(tipos.cuchillo)){
			vista.MostrarCuchilloEnPanelCentral();
		}
		
		else {
			if(validarPista(pista).equals(tipos.fuego)){
				vista.MostrarDisparoEnPanelCentral();
			}
			
			
			else{
				if(validarPista(pista).equals(tipos.ladron)){
					vista.informarLadronAtrapado();	
				}
				else{
					vista.MostrarPistaEnPanelCentral(pista);
				}
				}
		}
		
	}
	
	public void validarPistaRasgo(String pista){
		
		if(validarPista(pista).equals(tipos.cuchillo)){
					vista.MostrarCuchilloEnPanelCentral();
		}
		
		else {
			if(validarPista(pista).equals(tipos.fuego)){
				vista.MostrarDisparoEnPanelCentral();
			}
			
			
			else{
				if(validarPista(pista).equals(tipos.ladron)){
					vista.informarLadronAtrapado();
					vista.iniciarJuego();
				}
				else{
					vista.MostrarPistaRasgoEnPanelCentral(pista);
				}
				
			}
		}
		
	}
	
	private tipos validarPista(String pista) {
		if(pista.equalsIgnoreCase("Has sido acuchillado")){
			return tipos.cuchillo;
		}
		else{
			if(pista.equalsIgnoreCase("Te han disparado y has sido herido")){
				return tipos.fuego;
			}
			else{
				if(pista.equalsIgnoreCase("Ladron Atrapado")){
					return tipos.ladron;
				}
			}
		}
		return tipos.comun;
				
	}
	
}
