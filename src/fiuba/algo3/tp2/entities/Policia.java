package fiuba.algo3.tp2.entities;

import java.io.Serializable;
import java.util.Observable;

import fiuba.algo3.tp2.exceptions.ExcepcionCapitalesNoConectadas;
import fiuba.algo3.tp2.interfaces.Viajable;

public class Policia extends Observable implements Viajable, Serializable{
	
	


	private static final long serialVersionUID = 1L;
	private Tiempo tiempo;
	private Integer cantidad_arrestos;
	private String nombre;
	private SelectorRango rango;
	private Capital ubicacion;
	private OrdenDeArresto ordenDeArresto = new OrdenDeArresto();

	public Policia(String nombre, int cantidadArrestos) {
		this.nombre = nombre;
		tiempo = new Tiempo();
		rango = new SelectorRango();
		cantidad_arrestos = cantidadArrestos;
		inicializarRango();
		ubicacion = null;
	}
	
	public void inicializarRango(){
		
		if(cantidad_arrestos>4 && cantidad_arrestos<10){
			subirARangoDetective();
		}
		
		if (cantidad_arrestos > 9 && cantidad_arrestos < 20){
			subirARangoInvestigador();
		}
		if(cantidad_arrestos > 19){
			subirARangoSargento();
		}
	}
	
	
	public OrdenDeArresto getOrden(){
		return ordenDeArresto;
	}
	
	public String rango() {
		return rango.getNombreRango();
	}
	
	public String getNombre(){
		return nombre;
	}
	
	public Integer getVelocidad(){
		return rango.getVelocidadDesplazamiento();
	}
	
	public void subirARangoDetective(){
		rango.SubirRangoDetective();	
	}
	
	public void subirARangoSargento() {
		rango.SubirRangoSargento();
	}

	public void subirARangoInvestigador() {
		rango.SubirRangoInvestigador();
		
	}
	public boolean arrestoConseguido(){
		
		this.cantidad_arrestos++;	
		ModificadorPoliciaXml modificador = new ModificadorPoliciaXml();
		modificador.actualizar(nombre, cantidad_arrestos);
		ordenDeArresto = new OrdenDeArresto();
		if (this.cantidad_arrestos==5){
			subirARangoDetective();
			return true;
		}
		else if (this.cantidad_arrestos==10){
			subirARangoInvestigador();
			return true;
		}
		else if (this.cantidad_arrestos==20){
			subirARangoSargento();
			return true;
		}
		tiempo = new Tiempo();
		return false;
	}


	public void viajar(Capital destino) throws ExcepcionCapitalesNoConectadas {
		
		
		if (ubicacion == null) ubicacion = destino;
		else if (ubicacion.conexionExiste(destino.getNombreCapital())){
			
			int horas = this.calcularTiempoViaje(ubicacion.distanciaConexion(destino.getNombreCapital()));
			
			tiempo.avanzarHoras(horas);
			ubicacion = destino;
			actualizarObservadores();
		}
		
		else{
			 throw new ExcepcionCapitalesNoConectadas();
		}
		
		
	}

	public void viajarAInvestigarRobo(Capital destino)  {
		
		
		if (ubicacion == null) ubicacion = destino;
		ubicacion = destino;
		actualizarObservadores();	
	}
	
	private int calcularTiempoViaje(Integer distancia) {
		return new Double(distancia / this.getVelocidad()).intValue() + 1;
	}

	public void setUbicacion(Capital ubicacion) {
		this.ubicacion = ubicacion;
		
	}

	public Capital getUbicacion() {
		return ubicacion;
	}

	public void dormir() {
		tiempo.avanzarHoras(8);
		actualizarObservadores();
		
	}
	
	public boolean quedaTiempo(){
		return (tiempo.tiempoExpiro());
	}
	
	public String getHora(){
		return tiempo.actual();
	}

	public void pierdeTiempo(int i) {
		tiempo.avanzarHoras(i);
		actualizarObservadores();
		
	}
	
	public boolean estaCansado(){
		return tiempo.esNoche();
	}
	
	public void setearCapitalInicial(Capital origen) {
		ubicacion = origen;
	
	}

	public boolean verificarOrdenDeArresto() {
		tiempo.avanzarHoras(3);
		actualizarObservadores();
		return ordenDeArresto.ladronEncontrado();
		
	}
	
	public void actualizarObservadores() {
		 setChanged();
	     notifyObservers();
	}

	public void resetearTiempo() {
		tiempo = new Tiempo();		
	}
	public Integer getCantidad_arrestos() {
		return cantidad_arrestos;
	}

	public void setCantidad_arrestos(Integer cantidad_arrestos) {
		this.cantidad_arrestos = cantidad_arrestos;
		inicializarRango();
	}
	
	public Integer tiempoRestante(){
		return tiempo.tiempoRestante();
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
