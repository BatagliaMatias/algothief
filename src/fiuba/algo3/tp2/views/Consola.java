package fiuba.algo3.tp2.views;


import java.util.Scanner;

import fiuba.algo3.tp2.entities.*;
import fiuba.algo3.tp2.entities.LadronGenerico.TipoRasgo;
import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;

import java.util.Map;

public class Consola {
	
	public void darBienvenida(){
		System.out.println("Bienvenido a AlgoThief");
	}
	
	public void identificarPolicia(){
		System.out.println("Policia al teclado, identifiquese");
	    Scanner sc = new Scanner(System.in);
	    String nombre = sc.nextLine();
	    System.out.println("Se lo ha identificado como: "+nombre);
	    System.out.println("Es correcto? (Y/N)");
	    String respuesta = sc.nextLine().toLowerCase();
	    if(respuesta.equals("y")){
	    	
	    	this.iniciarJuego(nombre);
	    }
	    else{
	    	identificarPolicia();
	    }
	}
	
	public void iniciarJuego(String nombre){
		Juego juego = new Juego();
		Policia policia = new Policia(nombre,0);
		System.out.println("Tu graduacion actual es: "+ policia.rango());
		juego.agregarAgente(policia);
		juego.crearCaso();
		this.jugar(juego);
	}
	
	public void jugar(Juego juego){
		System.out.println("**Noticias**");
		//Capital capital = new Capital("Roma","Italia");
		Capital capital = juego.getCaso().getEscenario().obtenerCapital("Roma");
		//Capital capital = juego.getCaso().
		//Capital destino = new Capital("Buenos Aires","Argentina");
		Scanner sc = new Scanner(System.in);
		//juego.getCaso().setearCapital(capital);
		
		juego.ladronRobaObjetoEn();
		juego.policiaViajaA();
		
		//juego.policiaViajaA(capital);
		//juego.ladronRobaObjetoEn(capital);
		//juego.ladronViajaA(destino);
		System.out.println("Se ha robado un objeto en "+ juego.devolverNombreCapitalActual());
		System.out.println("El botin ha sido identificado como un "+ juego.devolverObjetoRobado());
		System.out.println("Un sospechoso de sexo "+ juego.devolverLadron().getRasgo(TipoRasgo.sexo).getDescripcion() +" ha sido visto en el lugar del crimen.");
		sc.nextLine();
		System.out.println("Tu mision: ");
		System.out.println("Perseguir al ladron desde "+ juego.devolverNombreCapitalActual() +" hasta su escondite y arrestarlo." );
		System.out.println("Tienes que arrestar al ladron antes del domingo a las 17hs.");
		System.out.println("Buena suerte "+ juego.getPolicia().rango() +" "+ juego.getPolicia().getNombre() );
		
		this.comenzarLaBusqueda(juego);
	
			
	}
		
	public void validarTiempo(Juego juego){
		if(!(juego.getPolicia().quedaTiempo())){
			System.out.println("Se agoto el tiempo!");
			cerrarCaso(juego);
		}
		
		if(juego.getPolicia().estaCansado()){
			juego.getPolicia().dormir();
			System.out.println("***Durmiendo***");
			mostrarUbicacionYTiempo(juego);
		}
	}
	
	public void mostrarUbicacionYTiempo(Juego juego){
		System.out.println("-----------------------------------------------------------------------");
		if(juego.getPolicia().quedaTiempo()){
			System.out.println("Ubicacion: "+ juego.getPolicia().getUbicacion().getNombreCapital() +" - "+ juego.getPolicia().getHora());
		}
		else{
			System.out.println("Se agoto el tiempo!");
			cerrarCaso(juego);
		}
		
	}
	
	
	
	
	public void comenzarLaBusqueda(Juego juego){
	
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Viajando...");
		if(juego.ladronPuedeViajar()){ // el ladron viaja en el inicio del juego, y cada vez que el policia viaja, siempre y cuando pueda viajar
			String capitalDestino = juego.getCaso().devolverLadron().determinarNombreProximaCiudad();
			Capital destino = juego.getCaso().getEscenario().obtenerCapital(capitalDestino);
			juego.ladronViajaA(destino);
		}
		else{
			juego.getCaso().devolverLadron().dejarTrampasYEsconderse(juego.getCaso().devolverLadron().getUbicacion());
		}
		sc.nextLine();
		String respuesta = "op";
		
		while(!respuesta.equals("salir") && !respuesta.equals("cerrar caso")){
		this.mostrarUbicacionYTiempo(juego);
		validarTiempo(juego);
		System.out.println("Que desea hacer? (ver paises relacionados/viajar/visitar lugar/cerrar caso)");
		respuesta = sc.nextLine().toLowerCase(); 
		
		validarTiempo(juego);
		
		/*	switch(respuesta){
			case "ver paises relacionados":
				this.mostrarPaisesRelacionados(juego);
				break;
			case "viajar":
				this.viajar(juego);
				break;
			case "visitar lugar":
				this.verPistas(juego);
				break;
			case "cerrar caso":
				this.cerrarCaso(juego);
				break;
			case "salir":
				
				break;
			}*/
		}
		
	}

	private void cerrarCaso(Juego juego) {
		Persistencia persistencia = new Persistencia();
		persistencia.guardarArchivo(juego);
		System.out.println("Caso cerrado");
		System.out.println("Desea volver a jugar? (Y/N)");
		Scanner sc = new Scanner(System.in);
		String respuesta = sc.nextLine().toLowerCase();
		if(respuesta.equals("y")){
			iniciarJuego(juego.getPolicia().getNombre());
		}
		else{
			
			System.out.println("Game over");
		}
		
	}


	private void verPistas(Juego juego) {
		// TODO Auto-generated method stub
		
		String respuesta = "";
		Scanner sc = new Scanner(System.in);
		Capital ubicacionActual = juego.getPolicia().getUbicacion();
		while(!respuesta.equals("salir")){
			validarTiempo(juego);
			System.out.println("Que lugar desea visitar?(Bolsa de valores/Aeropuerto/Calle/Museo)");
			System.out.println("o escriba salir para volver al menu anterior.");
			respuesta = sc.nextLine().toLowerCase();
			String pista = "me van a cambiar el valor";
			Policia policia = juego.getPolicia();
		/*	switch(respuesta){
			case "bolsa de valores":
				//pista = ubicacionActual.getDescripcionPista(TipoLugar.economico);
				pista = ubicacionActual.getPista(TipoLugar.economico).interactuar(policia);
				System.out.println(pista);
				System.out.println();
				break;
				
			case "aeropuerto":
				//pista = ubicacionActual.getDescripcionPista(TipoLugar.descriptivo);
				pista = ubicacionActual.getPista(TipoLugar.descriptivo).interactuar(policia);
				System.out.println(pista);
				System.out.println();
				break;
				
			case "calle":
			//	pista = ubicacionActual.getDescripcionPista(TipoLugar.rasgo);
				pista = ubicacionActual.getPista(TipoLugar.rasgo).interactuar(policia);
				System.out.println(pista);
				System.out.println();
				break;
			case "museo":
			//	pista = ubicacionActual.getDescripcionPista(TipoLugar.historico);
				pista = ubicacionActual.getPista(TipoLugar.historico).interactuar(policia);
				System.out.println(pista);
				System.out.println();
				break;
			}*/
		}
		
	}

	private void viajar(Juego juego) {
		// TODO Auto-generated method stub
		
		System.out.println("Desea viajar a: ");
		String respuesta = "op";
		Scanner sc = new Scanner(System.in);
		Map<String, Integer> conexiones = juego.getCaso().getCapitalActual().getConexiones();
	
		System.out.println(juego.getCaso().getCapitalActual().getConexiones());
		System.out.println("Escriba el nombre de la capital a la que desea viajar o salir para volver al menu anterior.");
		respuesta = sc.nextLine();
			if (!respuesta.equals("salir")){
				Capital destino = juego.getCaso().getEscenario().obtenerCapital(respuesta);
				if(juego.getCaso().getCapitalActual().conexionExiste(respuesta)){
					juego.policiaViajaA(destino);
					
					this.comenzarLaBusqueda(juego);
				}else System.out.println("No se puede viajar a esa capital, intente de nuevo.");	
			}else System.out.println("volviendo al menu anterior..");
			
			validarTiempo(juego);
		
	}

	private void mostrarPaisesRelacionados(Juego juego) {
		// TODO Auto-generated method stub
		
		System.out.println("Paises Relacionados:");
		System.out.println(juego.getCaso().getCapitalActual().getConexiones());
		
	}	
	
}
