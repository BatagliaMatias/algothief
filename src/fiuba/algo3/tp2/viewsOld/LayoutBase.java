package fiuba.algo3.tp2.viewsOld;

import javax.swing.JPanel;

public abstract class LayoutBase extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6866157368897371931L;
	
	
	protected static class Id implements IdLayout {
		
		private String id;
		
		public Id(String id) {
			this.id = id;
		}
		
		@Override
		public boolean equals(Object id) {
			return this.id.equals( ((Id)id).id );
		}
		
		@Override
		public String toString() {
			return this.id;
		}
		
	}

	public boolean identificar( IdLayout id ){
		return this.getId().equals(id);
	}
	
	abstract public IdLayout getId();
	
}
