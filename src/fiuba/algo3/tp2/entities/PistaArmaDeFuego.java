package fiuba.algo3.tp2.entities;

import java.io.Serializable;

import fiuba.algo3.tp2.interfaces.Pista;
import fiuba.algo3.tp2.interfaces.PistaTrampa;

public class PistaArmaDeFuego implements PistaTrampa , Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String interactuar(Policia policia) {
		policia.pierdeTiempo(1);
		policia.pierdeTiempo(4);
		return getDescripcion();
	}

	public void setDescripcion(Pista pista) {
		// TODO Auto-generated method stub
	}

	public String getDescripcion() {
		return "Te han disparado y has sido herido";
	}

}
