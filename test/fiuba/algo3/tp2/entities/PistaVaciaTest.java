package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.interfaces.Pista;

public class PistaVaciaTest {

	@Test
	public void testPistaVacia() {
		Pista pistaVacia = new PistaVacia();
		assertEquals(pistaVacia.getDescripcion(),"No he visto a nadie sospechoso");
	}

}
