package fiuba.algo3.tp2.entities;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ObjetoRobado implements Serializable {
	

	private static final long serialVersionUID = 1L;
	private String nombreObjeto;
	private String nombreCapital;
	
	public ObjetoRobado() {
		
		
		Document document;
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.parse(new File("xml/ObjetosRobables.xml"));
			NodeList nodeList = document.getDocumentElement().getChildNodes();
			int contadorNodo = 0;
			int random = randInt(1,11);
			for (int i = 0; i < nodeList.getLength(); i++) {
               Node node = nodeList.item(i);
               if (node.getNodeType() == Node.ELEMENT_NODE) {
            	 	  
             	  contadorNodo++;
             	  if(contadorNodo == random){  
                      setNombreObjeto(node.getAttributes().getNamedItem("NombreObjeto").getNodeValue());                    
                      setNombreCapital(node.getAttributes().getNamedItem("NombreCapital").getNodeValue());
             	 }                 
   
               }
			}
          
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	
	}
	
	
	
	public static int randInt(int min, int max) {

	    // Usually this should be a field rather than a method variable so
	    // that it is not re-seeded every call.
	    Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}


	
	public String getNombreObjeto() {
		return nombreObjeto;
	}


	public void setNombreObjeto(String nombreObjeto) {
		this.nombreObjeto = nombreObjeto;
	}

	public String getNombreCapital() {
		return nombreCapital;
	}


	public void setNombreCapital(String nombreCapital) {
		this.nombreCapital = nombreCapital;
	}

	
	public void leerObjetoDeCapital(Capital origen) {
		
		
		Document document;
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.parse(new File("xml/ObjetosRobables.xml"));
			NodeList nodeList = document.getDocumentElement().getChildNodes();
			for (int i = 0; i < nodeList.getLength(); i++) {
               Node node = nodeList.item(i);
               if (node.getNodeType() == Node.ELEMENT_NODE) {
            	 	  String nombreCapital= node.getAttributes().getNamedItem("NombreCapital").getNodeValue();
            	 	  if (nombreCapital.equals(origen.getNombreCapital())){
            	 		 setNombreObjeto(node.getAttributes().getNamedItem("NombreObjeto").getNodeValue());       
            	 		
            	 	  }
               
               }
			}
          
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	
	}
}
