package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.interfaces.Pista;

public class PistaRasgoTest {

	@Test
	public void testPistaRasgo() {
		Policia policia = new Policia("policia",0);
		Pista pista = new PistaRasgo();
		Rasgo rasgo = new Rasgo("pelado");
		((PistaRasgo) pista).setearRasgo(rasgo);
		pista.interactuar(policia);
		assertEquals(pista.interactuar(policia),rasgo.getDescripcion());
		assertTrue(policia.getOrden().ValidadRasgo(rasgo));
		
	}

}
