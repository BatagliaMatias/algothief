package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.entities.Tiempo;

public class TiempoTest {

	@Test
	public void testCrearTiempo() {
		Tiempo tiempo = new Tiempo();
		assertEquals(tiempo.actual(), "Lunes 7:00");
	}
	
	@Test
	public void testAvanzarDosHoras() {
		Tiempo tiempo = new Tiempo();
		int cantidadHoras = 2;
		tiempo.avanzarHoras(cantidadHoras);
		assertEquals(tiempo.actual(), "Lunes 9:00");
		
	}
	
	@Test
	public void testAvanzarUnDia() {
		Tiempo tiempo = new Tiempo();
		int cantidadHoras = 24;
		tiempo.avanzarHoras(cantidadHoras);
		assertEquals(tiempo.actual(), "Martes 7:00");
		
	}
	
	@Test
	public void testTiempoExpiro(){
		Tiempo tiempo = new Tiempo();
		int cantidadHoras = 24;
		tiempo.avanzarHoras(cantidadHoras); //martes
		tiempo.avanzarHoras(cantidadHoras); //miercoles
		tiempo.avanzarHoras(cantidadHoras); //jueves
		tiempo.avanzarHoras(cantidadHoras); //viernes
		tiempo.avanzarHoras(cantidadHoras); //sabado
		tiempo.avanzarHoras(cantidadHoras); //domingo 7:00
		tiempo.avanzarHoras(11);			//domingo 18:00
		assertFalse(tiempo.avanzarHoras(cantidadHoras));
	}

}
