package fiuba.algo3.tp2.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fiuba.algo3.tp2.entities.Juego;
import fiuba.algo3.tp2.entities.LectorPoliciaXml;
import fiuba.algo3.tp2.entities.Policia;
import fiuba.algo3.tp2.views.VentanaPrincipal;

public class ControladorSeleccionarPolicia implements ActionListener {
	
	private Juego juego;
	private VentanaPrincipal vista;
	private Policia policia;

	public ControladorSeleccionarPolicia(Juego juego, VentanaPrincipal ventanaPrincipal, Policia policia) {
		this.juego = juego;
		this.vista = ventanaPrincipal;
		this.policia = policia;
	}

	
	public void actionPerformed(ActionEvent e) {
		
		String nombre = e.getActionCommand();
	    LectorPoliciaXml lector = new LectorPoliciaXml();
	    policia.setNombre(nombre);
	    policia.setCantidad_arrestos(lector.leerCasos(nombre));    
	    vista.iniciarJuego();
	}


	public void setJuego(Juego juegoActual) {
		juego = juegoActual;
		policia = juegoActual.getPolicia();
		
	} 
}
