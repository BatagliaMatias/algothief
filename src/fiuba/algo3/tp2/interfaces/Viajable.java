package fiuba.algo3.tp2.interfaces;

import fiuba.algo3.tp2.entities.Capital;
import fiuba.algo3.tp2.exceptions.ExcepcionCapitalesNoConectadas;


public interface Viajable {
	
	public void viajar(Capital destino) throws ExcepcionCapitalesNoConectadas;
	
	public void setUbicacion(Capital ubicacion);
	
	public Capital getUbicacion();
}
