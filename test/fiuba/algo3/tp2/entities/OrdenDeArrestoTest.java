package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import fiuba.algo3.tp2.entities.LadronGenerico.TipoRasgo;

public class OrdenDeArrestoTest {

	@Test
	public void testCrearOrdenDeArresto() {
		OrdenDeArresto orden = new OrdenDeArresto();
		Rasgo rasgo = new Rasgo("femenino");
		orden.agregarRasgo(rasgo);
		assertTrue(orden.ValidadRasgo(rasgo));
	}
	
	@Test
	public void testValidarOrdenDeArrestoConLadron(){
		OrdenDeArresto orden = new OrdenDeArresto();
		String sexo = "Femenino";
		String hobby = "Tenis";
		String cabello = "Rubio";
		String senia = "Anillo";
		String vehiculo = "Moto";
		String nombre = "Nick Brunch";
		String dificultad = "facil";
		LadronGenerico ladronGenerico = new LadronGenerico(nombre,sexo,hobby,cabello,senia,vehiculo,dificultad);
		/*
		for(int i=0;i<6;i++){
			orden.agregarRasgo(ladronGenerico.getRasgo(0));
		}
		*/
		orden.agregarRasgo(ladronGenerico.getRasgo(TipoRasgo.cabello));
		orden.agregarRasgo(ladronGenerico.getRasgo(TipoRasgo.hobby));
		orden.agregarRasgo(ladronGenerico.getRasgo(TipoRasgo.sexo));
		orden.agregarRasgo(ladronGenerico.getRasgo(TipoRasgo.vehiculo));
		orden.agregarRasgo(ladronGenerico.getRasgo(TipoRasgo.senia));
		assertTrue(orden.ladronEncontrado());
		
		
	}
	
	@Test
	public void testValidarOrdenDeArrestoConLadronFalso(){
		OrdenDeArresto orden = new OrdenDeArresto();
		String sexo = "Femenino";
		String hobby = "Tenis";
		String cabello = "Rubio";
		String senia = "Anillo";
		String vehiculo = "Moto";
		String nombre = "Nick Brunch";
		String dificultad = "facil";
		LadronGenerico ladronGenerico = new LadronGenerico(nombre,sexo,hobby,cabello,senia,vehiculo,dificultad);
		/*
		for(int i=0;i<4;i++){
			orden.agregarRasgo(ladronGenerico.getRasgo(0));
		}
		*/
		orden.agregarRasgo(ladronGenerico.getRasgo(TipoRasgo.cabello));
		orden.agregarRasgo(ladronGenerico.getRasgo(TipoRasgo.hobby));
		orden.agregarRasgo(ladronGenerico.getRasgo(TipoRasgo.sexo));
		
		
		assertFalse(orden.ladronEncontrado());
		
		
	}

}
