package fiuba.algo3.tp2.viewsOld;

import java.awt.CardLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class VentanaInicio{

	JFrame frame;
	JPanel paneles;
	
	private HashMap<IdLayout, LayoutBase> layouts;

	public VentanaInicio() {
		
		frame = new JFrame("Algo-thief");
		frame.setSize(500, 500);
		this.layouts = new HashMap<IdLayout, LayoutBase>();
		this.initComponents();
		
		frame.setResizable(false);
		frame.addWindowListener( new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		frame.setVisible(false);
		
	}

	private void initComponents() {
		paneles = new JPanel(new CardLayout());
		frame.getContentPane().add(paneles);
	}
	
	public void agregarLayout(LayoutBase layout){
		paneles.add(layout, layout.getId().toString());
		this.layouts.put(layout.getId(), layout);
	}
	
	public void showLayout(IdLayout id){
		CardLayout cl = (CardLayout)(paneles.getLayout());
		cl.show(paneles, id.toString());
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);	
	}
	
	public LayoutBase getLayout(IdLayout id){
		return this.layouts.get(id);
	}
	
	public void ocultar() {
		frame.setVisible(false);
	}
	
}
