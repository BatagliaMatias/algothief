package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;


import fiuba.algo3.tp2.interfaces.Pista;

public class PistaEconomicaTest {

	@Test
	public void testPistaFacilEconomica(){
		Pista pistaEconomica = new PistaEconomica();
		LectorPistaXml lector = new LectorPistaXml();
		String dificultad = "facil";
		String tipo = "economico";
		String ciudad = "Roma";
		String esperado = "El sospechoso compro euros";
		pistaEconomica.setDescripcion(lector.leerPista(ciudad, dificultad, tipo));
		assertEquals(pistaEconomica.getDescripcion(), esperado);
				
	}
	
}
