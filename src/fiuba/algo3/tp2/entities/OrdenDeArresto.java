package fiuba.algo3.tp2.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Observable;


public class OrdenDeArresto extends Observable implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HashSet<Rasgo> rasgos;
	
	public OrdenDeArresto(){
		rasgos = new HashSet<Rasgo>();
	}

	public boolean ValidadRasgo(Rasgo rasgo) {
		return rasgos.contains(rasgo);
	}

	public void agregarRasgo(Rasgo rasgo) {
		rasgos.add(rasgo);
		this.actualizarControladores();
	}

	public boolean ladronEncontrado() {
		if (rasgos.size() >= 4){
			return true;
		}
		return false;
	}
	
	public HashSet<Rasgo> getRasgos(){
		return rasgos;
	}
	
	public int cantidadDeRasgosObtenidos(){
		return rasgos.size();
	}
	
	public void actualizarControladores() {
		 setChanged();
	     notifyObservers();
	}
}
