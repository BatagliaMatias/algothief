package fiuba.algo3.tp2.entities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import fiuba.algo3.tp2.interfaces.Lugar;
import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;
import fiuba.algo3.tp2.interfaces.Pista;

public class Capital implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nombreCapital;
	private String nombrePais;
	private Map<TipoLugar, Lugar> lugares = new HashMap<TipoLugar,Lugar>();
	private Map<String,Integer> conexiones = new HashMap<String,Integer>();

	public Capital(String capital, String nombrePais) {
		lugares.put(TipoLugar.economico,new LugarEconomico());
		lugares.put(TipoLugar.descriptivo,new LugarDescriptivo());		
		lugares.put(TipoLugar.historico,new LugarHistorico());
		lugares.put(TipoLugar.rasgo, new LugarRasgo());
		nombreCapital = capital;
		this.nombrePais = nombrePais;
		LectorCiudadConectadas lectorConectadas = new LectorCiudadConectadas();
		conexiones = lectorConectadas.leerConexiones(nombreCapital); //ver test si hay dudas
		
	}

	public String getNombreCapital() {
		return nombreCapital;
	}

	public String getNombrePais() {
		return nombrePais;
	}
	
	//Para los 2 metodos posteriores//
	//Ahora para obtener el lugar correspondiente se pide el tipo de lugar que es//
	public Lugar getLugar(TipoLugar tipo){
		return lugares.get(tipo);
	}

	public String getDescripcionPista(TipoLugar lugar) {
		return this.lugares.get(lugar).devolverPista();
	}
	
	public Pista getPista(TipoLugar lugar) {
		return this.lugares.get(lugar).devolverPistaCompleta();
	}

	public Map<String, Integer> getConexiones() {
		return conexiones;
	}

	public boolean conexionExiste(String nombreCapital) {
		return conexiones.containsKey(nombreCapital);
	}

	public Integer distanciaConexion(String nombreCapital) {
		return conexiones.get(nombreCapital);
	}
	
}
