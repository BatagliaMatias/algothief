package fiuba.algo3.tp2.entities;

import static org.junit.Assert.*;

import org.junit.Test;

public class RasgoTest {

	@Test
	public void testRasgo() {
		Rasgo rasgo = new Rasgo("rubia");
		assertEquals(rasgo.getDescripcion(),"rubia");
	}

}
