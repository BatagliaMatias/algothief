package fiuba.algo3.tp2.entities;

import java.io.Serializable;

import fiuba.algo3.tp2.interfaces.Lugar;
import fiuba.algo3.tp2.interfaces.Pista;

public class LugarRasgo implements Lugar , Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String descripcionLugar;
	private Pista pistaRasgo;
	
	public LugarRasgo(){
		descripcionLugar = "Calle";
	}
	
	
	public String devolverPista() {
		return pistaRasgo.getDescripcion();
	}

	public Pista devolverPistaCompleta() {
		return pistaRasgo;
	}

	public String getDescripcionLugar() {
		return descripcionLugar;
	}

	public void setearPista(Pista pista) {
		pistaRasgo = pista;

	}

	public TipoLugar getTipoLugar() {
		return TipoLugar.rasgo;
	}

}
