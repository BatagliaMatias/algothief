package fiuba.algo3.tp2.entities;

import java.io.Serializable;

import fiuba.algo3.tp2.interfaces.Rangos;

public class SelectorRango implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Rangos rangoPolicia;
	
	public SelectorRango(){
		rangoPolicia = new Novato();
		
	}
	
	public void SubirRangoDetective(){
		rangoPolicia = new Detective();
	}

	public void SubirRangoInvestigador(){
		rangoPolicia = new Investigador();
	}
	
	public void SubirRangoSargento(){
		rangoPolicia = new Sargento();
	}

	public Integer getVelocidadDesplazamiento(){
		
		return rangoPolicia.getVelocidadDesplazamiento();
	}
	
	public String getNombreRango(){
		
		return rangoPolicia.getNombreRango();
	}
	
}
