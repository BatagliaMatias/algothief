package fiuba.algo3.tp2.views;

import java.awt.Color;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fiuba.algo3.tp2.entities.Juego;

public class PanelCapitalesConectadas extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PanelCapitalesConectadas(Juego juego){
		
		Map<String,Integer> conexiones = juego.getPolicia().getUbicacion().getConexiones();
		//System.out.println("Capitales conectadas:");
		JLabel label = new JLabel("Capitales Conectadas: ");
		this.add(label);
			for (String key : conexiones.keySet()){
				mostrarConexion(key);
			}
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));	
		this.setVisible(true);
		setBackground(Color.LIGHT_GRAY);
	}

	private void mostrarConexion(String key) {
		// TODO Auto-generated method stub
		//System.out.println(key);
		JLabel label = new JLabel("	-" + key);
		this.add(label);
	}


}
