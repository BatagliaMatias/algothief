package fiuba.algo3.tp2.entities;
import java.io.Serializable;
import java.util.ArrayList;



public class Tiempo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final int tiempoTotal = 161;
	private ArrayList<String> dias;
	private int hora;
	private int dia;
	
	
	public Tiempo(){
		
		dias = new ArrayList<String>();
		hora = 7;
		dia = 0;
		dias.add("Lunes");
		dias.add("Martes");
		dias.add("Miercoles");
		dias.add("Jueves");
		dias.add("Viernes");
		dias.add("Sabado");
		dias.add("Domingo");
		dias.add("Lunes");
		dias.add("Martes");
		dias.add("Miercoles");
		
	}

	public String actual() {
		String salida = "";
		try{
			String auxDia = dias.get(dia);
			salida = auxDia +" "+ hora + ":00";
			return salida;
		}    
		catch(IndexOutOfBoundsException excepcion)
        {
	         System.out.println(" Error de �ndice en un array");
	         excepcion.printStackTrace();
	         return "Fin de tiempo";
        }
		
	}
	
	public boolean tiempoExpiro(){
		if(dia >= 6 && hora > 17 ){
			return false;
		}
		if(dia >=7){
			return false;
		}
		return true;
	}

	public boolean avanzarHoras(int avance) {
		if (hora+avance >= 24){
			dia = dia + 1;
			hora = hora + avance - 24;
		}
		else{
			hora = hora + avance;
		}
		
		return tiempoExpiro();
		
	}
	
	public Integer tiempoRestante(){
		return tiempoTotal-(dia*24)-hora;
	}
	
	public boolean esNoche(){
		if (hora < 6 || hora >= 22){
			if(tiempoExpiro()){
				return true;
			}
			
		}
		return false;
	}

}
