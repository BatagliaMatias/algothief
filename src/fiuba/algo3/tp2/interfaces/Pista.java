package fiuba.algo3.tp2.interfaces;

import fiuba.algo3.tp2.entities.Policia;

public interface Pista {
	
	String interactuar(Policia policia);
	
	void setDescripcion(Pista pista);

	String getDescripcion();

}
