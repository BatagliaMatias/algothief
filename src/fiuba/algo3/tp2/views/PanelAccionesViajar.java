package fiuba.algo3.tp2.views;

import java.awt.Color;

import java.util.Map;


import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

import fiuba.algo3.tp2.controllers.ControladorViajar;
import fiuba.algo3.tp2.entities.Juego;

public class PanelAccionesViajar extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ControladorViajar controlador;
	
	public PanelAccionesViajar(Juego juego, ControladorViajar controlador)
	
	{
		this.controlador = controlador;
		Map<String,Integer> conexiones = juego.getPolicia().getUbicacion().getConexiones();
		
			for (String key : conexiones.keySet()) {
				
				add(crearBoton(key + " a " + conexiones.get(key) +" KM"));
			}
		this.setVisible(true);
		setBackground(Color.LIGHT_GRAY);
		this.setBorder(BorderFactory.createMatteBorder(4,4,4,4,Color.BLACK));
	}	
	
	
	private JButton crearBoton(String nombre) { 
		JButton boton = new JButton(nombre);
		boton.addActionListener(controlador);
		return boton;
	}
}
