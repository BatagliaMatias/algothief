package fiuba.algo3.tp2.interfaces;

import fiuba.algo3.tp2.interfaces.Lugar.TipoLugar;

public interface CreadorPista {

	String darPista(String ciudad, TipoLugar lugar);
	
}